#define DLL_PROCESS_ATTACH   1    
#define DLL_THREAD_ATTACH    2    
#define DLL_THREAD_DETACH    3    
#define DLL_PROCESS_DETACH   0  
//==================================================================================================================================
#define DECLARE_HANDLE(name) struct name##__{int unused;}; typedef struct name##__ *name
//==================================================================================================================================
#define CONTAINING_RECORD(address, type, field) ((type *)( \
                                                  (PCHAR)(address) - \
                                                  (ULONG_PTR)(&((type *)0)->field)))
//==================================================================================================================================
/*
DECLARE_HANDLE(HINSTANCE);

typedef struct _MEMORY_BASIC_INFORMATION {
    PVOID BaseAddress;
    PVOID AllocationBase;
    DWORD AllocationProtect;
    SIZE_T RegionSize;
    DWORD State;
    DWORD Protect;
    DWORD Type;
} MEMORY_BASIC_INFORMATION, *PMEMORY_BASIC_INFORMATION;
*/
//==================================================================================================================================
#define PAGE_NOACCESS          0x01     
#define PAGE_READONLY          0x02     
#define PAGE_READWRITE         0x04     
#define PAGE_WRITECOPY         0x08     
#define PAGE_EXECUTE           0x10     
#define PAGE_EXECUTE_READ      0x20     
#define PAGE_EXECUTE_READWRITE 0x40     
#define PAGE_EXECUTE_WRITECOPY 0x80     
#define PAGE_GUARD            0x100     
#define PAGE_NOCACHE          0x200     
#define PAGE_WRITECOMBINE     0x400     
#define PAGE_REVERT_TO_FILE_MAP     0x80000000     
#define MEM_COMMIT                  0x1000      
#define MEM_RESERVE                 0x2000      
#define MEM_DECOMMIT                0x4000      
#define MEM_RELEASE                 0x8000      
#define MEM_FREE                    0x10000     
#define MEM_PRIVATE                 0x20000     
#define MEM_MAPPED                  0x40000     
#define MEM_RESET                   0x80000     
#define MEM_TOP_DOWN                0x100000    
#define MEM_WRITE_WATCH             0x200000    
#define MEM_PHYSICAL                0x400000    
#define MEM_ROTATE                  0x800000    
#define MEM_DIFFERENT_IMAGE_BASE_OK 0x800000    
#define MEM_RESET_UNDO              0x1000000   
#define MEM_LARGE_PAGES             0x20000000  
#define MEM_4MB_PAGES               0x80000000  
#define SEC_FILE           0x800000     
#define SEC_IMAGE         0x1000000     
#define SEC_PROTECTED_IMAGE  0x2000000  
#define SEC_RESERVE       0x4000000     
#define SEC_COMMIT        0x8000000     
#define SEC_NOCACHE      0x10000000     
#define SEC_WRITECOMBINE 0x40000000     
#define SEC_LARGE_PAGES  0x80000000     
#define SEC_IMAGE_NO_EXECUTE (SEC_IMAGE | SEC_NOCACHE)     
#define MEM_IMAGE         SEC_IMAGE     
#define WRITE_WATCH_FLAG_RESET  0x01     
#define MEM_UNMAP_WITH_TRANSIENT_BOOST  0x01    
//==================================================================================================================================
DECLARE_HANDLE(HINSTANCE);
//==================================================================================================================================
typedef HINSTANCE HMODULE;
//==================================================================================================================================
#define WINAPI __stdcall
#define PASCAL __stdcall
#define RESTRICTED_POINTER __restrict
#define NTAPI __stdcall
//==================================================================================================================================
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef __int64 QWORD;
//==================================================================================================================================
typedef BYTE* PBYTE;
typedef WORD* PWORD;
typedef DWORD* PDWORD;
//==================================================================================================================================
typedef char CHAR;
typedef short SHORT;
typedef long LONG;
typedef void VOID;
//==================================================================================================================================
typedef CHAR* PCHAR;
typedef SHORT* PSHORT;
typedef LONG* PLONG;
typedef void* PVOID;
//==================================================================================================================================
typedef unsigned char UCHAR;
typedef unsigned short USHORT;
typedef unsigned long ULONG;
//==================================================================================================================================
typedef UCHAR* PUCHAR;
typedef USHORT* PUSHORT;
typedef ULONG* PULONG;
//==================================================================================================================================
typedef UCHAR* UCHAR_PTR;
typedef USHORT* USHORT_PTR;
typedef ULONG ULONG_PTR;
//==================================================================================================================================
typedef wchar_t WCHAR;
typedef WCHAR* PWSTR;
//==================================================================================================================================
typedef int BOOL;
typedef int INT;
typedef float FLOAT;
typedef double DOUBLE;
typedef BYTE BOOLEAN;
//==================================================================================================================================
typedef BOOL* PBOOL;
typedef INT* PINT;
typedef FLOAT* PFLOAT;
typedef DOUBLE* PDOUBLE;
//==================================================================================================================================
typedef signed char SBYTE;
typedef SBYTE *PSBYTE;
//==================================================================================================================================
typedef signed short SWORD;
typedef SWORD *PSWORD;
//==================================================================================================================================
typedef signed long SDWORD;
typedef SDWORD *PSDWORD;
//==================================================================================================================================
typedef unsigned int UINT;
typedef UINT* PUINT;
//==================================================================================================================================
typedef void* LPVOID;
typedef const void* LPCVOID;
//==================================================================================================================================
typedef BOOLEAN* PBOOLEAN;
//==================================================================================================================================
typedef unsigned int uint32;
//==================================================================================================================================
typedef const CHAR* LPCSTR;
typedef LPCSTR LPCTSTR;
//==================================================================================================================================
typedef DWORD* LPDWORD;
//==================================================================================================================================
typedef CHAR* LPSTR;
//==================================================================================================================================
typedef LPSTR LPTSTR;
//==================================================================================================================================
typedef void* HANDLE;
//==================================================================================================================================
typedef HANDLE* PHANDLE;
//==================================================================================================================================
typedef INT SIZE_T;
typedef SIZE_T* PSIZE_T;
//==================================================================================================================================
typedef LONG NTSTATUS;
//==================================================================================================================================
#define MAX_PATH 260
//==================================================================================================================================
#define NULL 0
//==================================================================================================================================
typedef   struct _UNICODE_STRING {
  USHORT  Length;
  USHORT  MaximumLength;
  PWSTR  Buffer;
} UNICODE_STRING, *PUNICODE_STRING;
//==================================================================================================================================
typedef struct CLIENT_ID 
{
    DWORD UniqueProcess;
    DWORD UniqueThread;
} CLIENT_ID , *PCLIENT_ID;
//==================================================================================================================================
typedef struct _OBJECT_ATTRIBUTES {
  ULONG           Length;
  HANDLE          RootDirectory;
  PUNICODE_STRING ObjectName;
  ULONG           Attributes;
  PVOID           SecurityDescriptor;
  PVOID           SecurityQualityOfService;
}  OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;
//==================================================================================================================================
typedef struct _LIST_ENTRY {
   struct _LIST_ENTRY *Flink;
   struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY;
//==================================================================================================================================
typedef struct _LDR_DATA_TABLE_ENTRY {
	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
	PVOID                   BaseAddress;
	PVOID                   EntryPoint;
	ULONG                   SizeOfImage;
	UNICODE_STRING          FullDllName;
	UNICODE_STRING          BaseDllName;
	ULONG                   Flags;
	SHORT                   LoadCount;
	SHORT                   TlsIndex;
	LIST_ENTRY              HashTableEntry;
	ULONG                   TimeDateStamp;
} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;
//==================================================================================================================================
typedef struct _RTL_USER_PROCESS_PARAMETERS {
  BYTE           Reserved1[16];
  PVOID          Reserved2[10];
  UNICODE_STRING ImagePathName;
  UNICODE_STRING CommandLine;
} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;
//==================================================================================================================================
typedef struct _PEB_LDR_DATA {
  ULONG                   Length;
  BOOLEAN                 Initialized;
  PVOID                   SsHandle;
  LIST_ENTRY              InLoadOrderModuleList;
  LIST_ENTRY              InMemoryOrderModuleList;
  LIST_ENTRY              InInitializationOrderModuleList;
} PEB_LDR_DATA, *PPEB_LDR_DATA;
//==================================================================================================================================
typedef struct _PROCESS_ENVIRONMENT_BLOCK {
  BYTE                          Reserved1[2];
  BYTE                          BeingDebugged;
  BYTE                          Reserved2[1];
  PVOID                         Reserved3[2];
  PPEB_LDR_DATA                 Ldr;
  PRTL_USER_PROCESS_PARAMETERS  ProcessParameters;
  BYTE                          Reserved4[104];
  PVOID                         Reserved5[52];
  PVOID							PostProcessInitRoutine;
//PPS_POST_PROCESS_INIT_ROUTINE PostProcessInitRoutine;
  BYTE                          Reserved6[128];
  PVOID                         Reserved7[1];
  ULONG                         SessionId;
} PROCESS_ENVIRONMENT_BLOCK, *PPROCESS_ENVIRONMENT_BLOCK;
//==================================================================================================================================
typedef struct _TEB {

  /*NT_TIB*/PVOID								Tib;
  PVOID											EnvironmentPointer;
  CLIENT_ID										Cid;
  PVOID											ActiveRpcInfo;
  PVOID											ThreadLocalStoragePointer;
  PROCESS_ENVIRONMENT_BLOCK*                    Peb;
  ULONG											LastErrorValue;
  ULONG											CountOfOwnedCriticalSections;
  PVOID											CsrClientThread;
  PVOID											Win32ThreadInfo;
  ULONG											Win32ClientInfo[0x1F];
  PVOID											WOW32Reserved;
  ULONG											CurrentLocale;
  ULONG											FpSoftwareStatusRegister;
  PVOID											SystemReserved1[0x36];
  PVOID											Spare1;
  ULONG											ExceptionCode;
  ULONG											SpareBytes1[0x28];
  PVOID											SystemReserved2[0xA];
  ULONG											GdiRgn;
  ULONG											GdiPen;
  ULONG											GdiBrush;
  CLIENT_ID										RealClientId;
  PVOID											GdiCachedProcessHandle;
  ULONG											GdiClientPID;
  ULONG											GdiClientTID;
  PVOID											GdiThreadLocaleInfo;
  PVOID											UserReserved[5];
  PVOID											GlDispatchTable[0x118];
  ULONG											GlReserved1[0x1A];
  PVOID											GlReserved2;
  PVOID											GlSectionInfo;
  PVOID											GlSection;
  PVOID											GlTable;
  PVOID											GlCurrentRC;
  PVOID											GlContext;
  NTSTATUS										LastStatusValue;
  UNICODE_STRING								StaticUnicodeString;
  WCHAR											StaticUnicodeBuffer[0x105];
  PVOID											DeallocationStack;
  PVOID											TlsSlots[0x40];
  LIST_ENTRY									TlsLinks;
  PVOID											Vdm;
  PVOID											ReservedForNtRpc;
  PVOID											DbgSsReserved[0x2];
  ULONG											HardErrorDisabled;
  PVOID											Instrumentation[0x10];
  PVOID											WinSockData;
  ULONG											GdiBatchCount;
  ULONG											Spare2;
  ULONG											Spare3;
  ULONG											Spare4;
  PVOID											ReservedForOle;
  ULONG											WaitingOnLoaderLock;
  PVOID											StackCommit;
  PVOID											StackCommitMax;
  PVOID											StackReserved;
} TEB, *PTEB;

//==================================================================================================================================
#define IMAGE_DOS_SIGNATURE                 0x5A4D      // MZ
#define IMAGE_NT_SIGNATURE                  0x00004550  // PE00
//==================================================================================================================================
typedef struct _IMAGE_DOS_HEADER {      // DOS .EXE header
    WORD   e_magic;                     // Magic number
    WORD   e_cblp;                      // Bytes on last page of file
    WORD   e_cp;                        // Pages in file
    WORD   e_crlc;                      // Relocations
    WORD   e_cparhdr;                   // Size of header in paragraphs
    WORD   e_minalloc;                  // Minimum extra paragraphs needed
    WORD   e_maxalloc;                  // Maximum extra paragraphs needed
    WORD   e_ss;                        // Initial (relative) SS value
    WORD   e_sp;                        // Initial SP value
    WORD   e_csum;                      // Checksum
    WORD   e_ip;                        // Initial IP value
    WORD   e_cs;                        // Initial (relative) CS value
    WORD   e_lfarlc;                    // File address of relocation table
    WORD   e_ovno;                      // Overlay number
    WORD   e_res[4];                    // Reserved words
    WORD   e_oemid;                     // OEM identifier (for e_oeminfo)
    WORD   e_oeminfo;                   // OEM information; e_oemid specific
    WORD   e_res2[10];                  // Reserved words
    LONG   e_lfanew;                    // File address of new exe header
  } IMAGE_DOS_HEADER, *PIMAGE_DOS_HEADER;
//==================================================================================================================================
typedef struct _IMAGE_FILE_HEADER {
    WORD    Machine;
    WORD    NumberOfSections;
    DWORD   TimeDateStamp;
    DWORD   PointerToSymbolTable;
    DWORD   NumberOfSymbols;
    WORD    SizeOfOptionalHeader;
    WORD    Characteristics;
} IMAGE_FILE_HEADER, *PIMAGE_FILE_HEADER;
//==================================================================================================================================
#define IMAGE_SIZEOF_FILE_HEADER             20
//==================================================================================================================================
typedef struct _IMAGE_DATA_DIRECTORY {
    DWORD   VirtualAddress;
    DWORD   Size;
} IMAGE_DATA_DIRECTORY, *PIMAGE_DATA_DIRECTORY;
//==================================================================================================================================
#define IMAGE_NUMBEROF_DIRECTORY_ENTRIES    16
//==================================================================================================================================
//
// Optional header format.
//
//==================================================================================================================================
typedef struct _IMAGE_OPTIONAL_HEADER {
    //
    // Standard fields.
    //

    WORD    Magic;
    BYTE    MajorLinkerVersion;
    BYTE    MinorLinkerVersion;
    DWORD   SizeOfCode;
    DWORD   SizeOfInitializedData;
    DWORD   SizeOfUninitializedData;
    DWORD   AddressOfEntryPoint;
    DWORD   BaseOfCode;
    DWORD   BaseOfData;

    //
    // NT additional fields.
    //

    DWORD   ImageBase;
    DWORD   SectionAlignment;
    DWORD   FileAlignment;
    WORD    MajorOperatingSystemVersion;
    WORD    MinorOperatingSystemVersion;
    WORD    MajorImageVersion;
    WORD    MinorImageVersion;
    WORD    MajorSubsystemVersion;
    WORD    MinorSubsystemVersion;
    DWORD   Win32VersionValue;
    DWORD   SizeOfImage;
    DWORD   SizeOfHeaders;
    DWORD   CheckSum;
    WORD    Subsystem;
    WORD    DllCharacteristics;
    DWORD   SizeOfStackReserve;
    DWORD   SizeOfStackCommit;
    DWORD   SizeOfHeapReserve;
    DWORD   SizeOfHeapCommit;
    DWORD   LoaderFlags;
    DWORD   NumberOfRvaAndSizes;
    IMAGE_DATA_DIRECTORY DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
} IMAGE_OPTIONAL_HEADER32, *PIMAGE_OPTIONAL_HEADER32;
//==================================================================================================================================
typedef struct _IMAGE_NT_HEADERS {
    DWORD Signature;
    IMAGE_FILE_HEADER FileHeader;
    IMAGE_OPTIONAL_HEADER32 OptionalHeader;
} IMAGE_NT_HEADERS32, *PIMAGE_NT_HEADERS32;
//==================================================================================================================================
typedef struct _IMAGE_EXPORT_DIRECTORY {
    DWORD   Characteristics;
    DWORD   TimeDateStamp;
    WORD    MajorVersion;
    WORD    MinorVersion;
    DWORD   Name;
    DWORD   Base;
    DWORD   NumberOfFunctions;
    DWORD   NumberOfNames;
    DWORD   AddressOfFunctions;     // RVA from base of image
    DWORD   AddressOfNames;         // RVA from base of image
    DWORD   AddressOfNameOrdinals;  // RVA from base of image
} IMAGE_EXPORT_DIRECTORY, *PIMAGE_EXPORT_DIRECTORY;
//==================================================================================================================================
typedef IMAGE_NT_HEADERS32                  IMAGE_NT_HEADERS;
typedef PIMAGE_NT_HEADERS32                 PIMAGE_NT_HEADERS;
//==================================================================================================================================
#define IMAGE_DIRECTORY_ENTRY_EXPORT          0   // Export Directory
#define IMAGE_DIRECTORY_ENTRY_IMPORT          1   // Import Directory
#define IMAGE_DIRECTORY_ENTRY_RESOURCE        2   // Resource Directory
#define IMAGE_DIRECTORY_ENTRY_EXCEPTION       3   // Exception Directory
#define IMAGE_DIRECTORY_ENTRY_SECURITY        4   // Security Directory
#define IMAGE_DIRECTORY_ENTRY_BASERELOC       5   // Base Relocation Table
#define IMAGE_DIRECTORY_ENTRY_DEBUG           6   // Debug Directory
//      IMAGE_DIRECTORY_ENTRY_COPYRIGHT       7   // (X86 usage)
#define IMAGE_DIRECTORY_ENTRY_ARCHITECTURE    7   // Architecture Specific Data
#define IMAGE_DIRECTORY_ENTRY_GLOBALPTR       8   // RVA of GP
#define IMAGE_DIRECTORY_ENTRY_TLS             9   // TLS Directory
#define IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG    10   // Load Configuration Directory
#define IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT   11   // Bound Import Directory in headers
#define IMAGE_DIRECTORY_ENTRY_IAT            12   // Import Address Table
#define IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT   13   // Delay Load Import Descriptors
#define IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR 14   // COM Runtime descriptor
//==================================================================================================================================
#define VK_SPACE          0x20
#define VK_PRIOR          0x21
#define VK_NEXT           0x22
#define VK_END            0x23
#define VK_HOME           0x24
#define VK_LEFT           0x25
#define VK_UP             0x26
#define VK_RIGHT          0x27
#define VK_DOWN           0x28
#define VK_SELECT         0x29
#define VK_PRINT          0x2A
#define VK_EXECUTE        0x2B
#define VK_SNAPSHOT       0x2C
#define VK_INSERT         0x2D
#define VK_DELETE         0x2E
#define VK_HELP           0x2F
//==================================================================================================================================
#define VK_DIVIDE         0x6F
//==================================================================================================================================
#define VK_NUMLOCK        0x90
#define VK_SCROLL         0x91
//==================================================================================================================================
typedef struct tagPOINT {
  LONG x;
  LONG y;
} POINT, *PPOINT;
//==================================================================================================================================
PROCESS_ENVIRONMENT_BLOCK* GetPEB ( void );
//==================================================================================================================================