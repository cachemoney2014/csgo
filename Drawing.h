//==================================================================================================================================
#define AlignRight ( 1 << 0 )
#define AlignCenterH ( 1 << 1 )
#define AlignCenterV ( 1 << 2 )
//==================================================================================================================================
typedef struct Color_s
{
	Color_s()
	{
		
	}

	Color_s ( unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha ) : r(red), g(green), b(blue), a(alpha) 
	{
	}

	void Init ( unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha )
	{
		r = red;
		g = green;
		b = blue;
		a = alpha;
	}

	unsigned char r, g, b, a;
}Color_t;
//==================================================================================================================================
class CDraw
{
public:
	uintptr_t m_MenuFont;
	uintptr_t m_ESPFont;

public:

	float GetSliderStep ( float MaxValue, int Width );

	void DrawString ( int x, int y, Color_t& Color, char* Text, uintptr_t Align, uintptr_t Font );

	void DrawStringEncrypted ( int x, int y, Color_t& Color, char* Text, uintptr_t Length, uintptr_t Key, int& Width, int& Height, uintptr_t Align, uintptr_t Font );

	void DrawFilledRect ( int x, int y, int w, int h, Color_t& Color );
	void DrawLine ( int x, int y, int x1, int y1, Color_t& Color );
	void DrawOutlinedRect ( int x, int y, int w, int h, Color_t& Color );

	void GradientV ( int x, int y, int w, int h, Color_t& c1, Color_t& c2 );
	void GradientH ( int x, int y, int w, int h, Color_t& c1, Color_t& c2 );
};
//==================================================================================================================================
#define ESP_FONT_SIZE 16
#define MENU_FONT_SIZE 12
//==================================================================================================================================
extern CDraw g_Draw;
//==================================================================================================================================