enum ButtonCode_t
{
	BUTTON_CODE_INVALID = -1,
	BUTTON_CODE_NONE = 0,

	KEY_FIRST = 0,

	KEY_NONE = KEY_FIRST,
	KEY_0,
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,
	KEY_A,
	KEY_B,
	KEY_C,
	KEY_D,
	KEY_E,
	KEY_F,
	KEY_G,
	KEY_H,
	KEY_I,
	KEY_J,
	KEY_K,
	KEY_L,
	KEY_M,
	KEY_N,
	KEY_O,
	KEY_P,
	KEY_Q,
	KEY_R,
	KEY_S,
	KEY_T,
	KEY_U,
	KEY_V,
	KEY_W,
	KEY_X,
	KEY_Y,
	KEY_Z,
	KEY_PAD_0,
	KEY_PAD_1,
	KEY_PAD_2,
	KEY_PAD_3,
	KEY_PAD_4,
	KEY_PAD_5,
	KEY_PAD_6,
	KEY_PAD_7,
	KEY_PAD_8,
	KEY_PAD_9,
	KEY_PAD_DIVIDE,
	KEY_PAD_MULTIPLY,
	KEY_PAD_MINUS,
	KEY_PAD_PLUS,
	KEY_PAD_ENTER,
	KEY_PAD_DECIMAL,
	KEY_LBRACKET,
	KEY_RBRACKET,
	KEY_SEMICOLON,
	KEY_APOSTROPHE,
	KEY_BACKQUOTE,
	KEY_COMMA,
	KEY_PERIOD,
	KEY_SLASH,
	KEY_BACKSLASH,
	KEY_MINUS,
	KEY_EQUAL,
	KEY_ENTER,
	KEY_SPACE,
	KEY_BACKSPACE,
	KEY_TAB,
	KEY_CAPSLOCK,
	KEY_NUMLOCK,
	KEY_ESCAPE,
	KEY_SCROLLLOCK,
	KEY_INSERT,
	KEY_DELETE,
	KEY_HOME,
	KEY_END,
	KEY_PAGEUP,
	KEY_PAGEDOWN,
	KEY_BREAK,
	KEY_LSHIFT,
	KEY_RSHIFT,
	KEY_LALT,
	KEY_RALT,
	KEY_LCONTROL,
	KEY_RCONTROL,
	KEY_LWIN,
	KEY_RWIN,
	KEY_APP,
	KEY_UP,
	KEY_LEFT,
	KEY_DOWN,
	KEY_RIGHT,
	KEY_F1,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	KEY_F10,
	KEY_F11,
	KEY_F12,
	KEY_CAPSLOCKTOGGLE,
	KEY_NUMLOCKTOGGLE,
	KEY_SCROLLLOCKTOGGLE,

	KEY_LAST = KEY_SCROLLLOCKTOGGLE,
	KEY_COUNT = KEY_LAST - KEY_FIRST + 1,

	// Mouse
	MOUSE_FIRST = KEY_LAST + 1,

	MOUSE_LEFT = MOUSE_FIRST,
	MOUSE_RIGHT,
	MOUSE_MIDDLE,
	MOUSE_4,
	MOUSE_5,
	MOUSE_WHEEL_UP,		// A fake button which is 'pressed' and 'released' when the wheel is moved up 
	MOUSE_WHEEL_DOWN,	// A fake button which is 'pressed' and 'released' when the wheel is moved down

	MOUSE_LAST = MOUSE_WHEEL_DOWN,
	MOUSE_COUNT = MOUSE_LAST - MOUSE_FIRST + 1,

	
};

#define IN_ATTACK		(1 << 0)
#define IN_JUMP			(1 << 1)
#define IN_DUCK			(1 << 2)
#define IN_FORWARD		(1 << 3)
#define IN_BACK			(1 << 4)
#define IN_USE			(1 << 5)
#define IN_CANCEL		(1 << 6)
#define IN_LEFT			(1 << 7)
#define IN_RIGHT		(1 << 8)
#define IN_MOVELEFT		(1 << 9)
#define IN_MOVERIGHT	(1 << 10)
#define IN_ATTACK2		(1 << 11)
#define IN_RUN			(1 << 12)
#define IN_RELOAD		(1 << 13)
#define IN_ALT1			(1 << 14)
#define IN_ALT2			(1 << 15)
#define IN_SCORE		(1 << 16)   // Used by client.dll for when scoreboard is held down
#define IN_SPEED		(1 << 17)	// Player is holding the speed key
#define IN_WALK			(1 << 18)	// Player holding walk key
#define IN_ZOOM			(1 << 19)	// Zoom key for HUD zoom
#define IN_WEAPON1		(1 << 20)	// weapon defines these bits
#define IN_WEAPON2		(1 << 21)	// weapon defines these bits
#define IN_BULLRUSH		(1 << 22)
#define IN_GRENADE1		(1 << 23)	// grenade 1
#define IN_GRENADE2		(1 << 24)	// grenade 2
#define	IN_ATTACK3		(1 << 25)

#define	FL_ONGROUND		(1<<0)	// At rest / on the ground
#define FL_DUCKING		(1<<1)	// Player flag -- Player is fully crouched

enum ClientFrameStage_t
{
	FRAME_UNDEFINED = -1,
	FRAME_START,
	FRAME_NET_UPDATE_START,
	FRAME_NET_UPDATE_POSTDATAUPDATE_START,
	FRAME_NET_UPDATE_POSTDATAUPDATE_END,
	FRAME_NET_UPDATE_END,
	FRAME_RENDER_START,
	FRAME_RENDER_END
};

enum ClassIds_t
{
	CC4_Id = 29,
	CChicken = 31,
};

class CEntityGroundContact
{
public:
	int					entindex;
	float				minheight;
	float				maxheight;
};

class CUserCmd
{
public:
	CUserCmd()
	{
		
	}
	
	CUserCmd& operator =( const CUserCmd& src )
	{
		if ( this == &src )
			return *this;

		command_number		= src.command_number;
		tick_count			= src.tick_count;
		viewangles			= src.viewangles;
		aimdirection		= src.aimdirection;
		forwardmove			= src.forwardmove;
		sidemove			= src.sidemove;
		upmove				= src.upmove;
		buttons				= src.buttons;
		impulse				= src.impulse;
		weaponselect		= src.weaponselect;
		weaponsubtype		= src.weaponsubtype;
		random_seed			= src.random_seed;
		mousedx				= src.mousedx;
		mousedy				= src.mousedy;

		hasbeenpredicted	= src.hasbeenpredicted;

#if defined( HL2_DLL ) || defined( HL2_CLIENT_DLL )
		entitygroundcontact			= src.entitygroundcontact;
#endif



		// TrackIR
		headangles			= src.headangles;
		headoffset			= src.headoffset;
		// TrackIR

#if defined( INFESTED_DLL )
		crosshairtrace		= src.crosshairtrace;
#endif

#ifdef INFESTED_DLL
		crosshair_entity			= src.crosshair_entity;
		forced_action				= src.forced_action;
		sync_kill_ent				= src.sync_kill_ent;
		skill_dest					= src.skill_dest;
		skill_dest_ent				= src.skill_dest_ent;
#endif

		return *this;
	}

	CUserCmd( const CUserCmd& src )
	{
		*this = src;
	}
	
	// For matching server and client commands for debugging
	int		command_number;
	
	// the tick the client created this command
	int		tick_count;
	
	// Player instantaneous view angles.
	QAngle	viewangles;     
	
	Vector	aimdirection;
	
	// Intended velocities
	//	forward velocity.
	float	forwardmove;   
	//  sideways velocity.
	float	sidemove;      
	//  upward velocity.
	float	upmove;         
	// Attack button states
	int		buttons;		
	// Impulse command issued.
	unsigned char    impulse;        
	// Current weapon id
	int		weaponselect;	
	int		weaponsubtype;

	int		random_seed;	// For shared random functions

	short	mousedx;		// mouse accum in x from create move
	short	mousedy;		// mouse accum in y from create move

	// Client only, tracks whether we've predicted this command at least once
	bool	hasbeenpredicted;

	// Back channel to communicate IK state
#if defined( HL2_DLL ) || defined( HL2_CLIENT_DLL )
	CUtlVector< CEntityGroundContact > entitygroundcontact;
#endif


	// TrackIR
	QAngle headangles;
	Vector headoffset;
	// TrackIR

#if defined( INFESTED_DLL )
	Vector crosshairtrace;		// world location directly beneath the player's crosshair
#endif

#ifdef INFESTED_DLL
	short crosshair_entity;			// index of the entity under the player's crosshair
	byte forced_action;
	short sync_kill_ent;
	Vector skill_dest;
	short skill_dest_ent;
#endif
};

struct RecvProp;

class DVariant
{
public:
	union
	{
		float   m_Float;        //0x00
		long    m_Int;          //0x04 
		char*   m_pString;      //0x08
		void*   m_pData;        //0x0C
		float   m_Vector[ 3 ];  //0x10
		__int64 m_Int64;        //0x1C
	};//Size=0x24
 
	int m_Type; //0x24
 
};//Size=0x28
 
class CRecvProxyData
{
public:
	RecvProp*		m_pRecvProp; //0x00
	DVariant		m_Value;     //0x04
	int				m_iElement;  //0x2C
	int				m_iObjectId; //0x30
};
 
struct C_BaseEntity
{
public:

	const Vector& GetRenderOrigin()
	{
		PVOID pRenderable = (void*)((uintptr_t)this + 0x4);
		typedef const Vector& (__thiscall* oRenderOrigin)(PVOID);
		return CallVirtual< oRenderOrigin >(pRenderable, 1)(pRenderable);
	}

	Vector& GetRenderAngles()
	{
		PVOID pRenderable = (void*)((uintptr_t)this + 0x4);
		typedef Vector& (__thiscall* oRenderAngles)(PVOID);
		return CallVirtual< oRenderAngles >(pRenderable, 2)(pRenderable);
	}
	
	bool SetupBones(matrix3x4_t* matrix, int maxbones, int mask, float time)
	{
		PVOID pRenderable = (PVOID)((uintptr_t)this + 0x4);
		typedef bool(__thiscall* oSetupBones)(PVOID, matrix3x4_t*, int, int, float);
		return CallVirtual< oSetupBones >(pRenderable, 13)(pRenderable, matrix, maxbones, mask, time);
	}
	
	bool IsDormant()
	{
		PVOID pNetworkable = (void*)((uintptr_t)this + 0x8);
		typedef bool(__thiscall*tOrig_IsDormantFn)(PVOID);
		return CallVirtual<tOrig_IsDormantFn>(pNetworkable, 9)(pNetworkable);
	}
	
	int GetIndex()
	{
		PVOID pNetworkable = (void*)((uintptr_t)this + 0x8);
		typedef int(__thiscall* oIndex)(PVOID);
		return CallVirtual< oIndex >(pNetworkable, 10)(pNetworkable);
	}
};

typedef struct player_info_s {
private:
	DWORD __pad0[2];
public:
	int m_nXuidLow;
	int m_nXuidHigh;
	char name[128];
	int userId;
	char steamId[33];
	UINT m_nSteam3ID;
	char friendName[128];
	bool fake_player;
	bool isHLTV;
	DWORD customFiles[4];
	BYTE fileDownloaded;
private:
	int __pad1;
} player_info_t;

class IVEngineClient
{
public:

	void GetScreenSize(int& width, int& height)
	{
		typedef void(__thiscall* oScreenSize)(PVOID, int&, int&);
		return CallVirtual< oScreenSize >(this, 5)(this, width, height);
	}

	bool GetPlayerInfo(int ent_num, player_info_t *pinfo)
	{
		typedef bool(__thiscall* oPlayerInfo)(PVOID, int, player_info_t*);
		return CallVirtual< oPlayerInfo>(this, 8)(this, ent_num, pinfo);
	}

	int GetLocalPlayer(void)
	{
		typedef int(__thiscall* oLocalPlayer)(PVOID);
		return CallVirtual< oLocalPlayer >(this, 12)(this);
	}

	void GetViewAngles(Vector&va)
	{
		typedef void(__thiscall* oGetViewAngles)(PVOID, Vector&);
		return CallVirtual< oGetViewAngles >(this, 18)(this, va);
	}

	void SetViewAngles(Vector&va)
	{
		typedef void(__thiscall* oSetViewAngles)(PVOID, Vector&);
		return CallVirtual< oSetViewAngles >(this, 19)(this, va);
	}

	int GetMaxClients()
	{
		typedef int(__thiscall* oMaxClients)(PVOID);
		return CallVirtual< oMaxClients >(this, 20)(this);
	}

	bool IsInGame()
	{
		typedef bool(__thiscall* oInGame)(PVOID);
		return CallVirtual< oInGame >(this, 26)(this);
	}

	const VMatrix& WorldToScreenMatrix()
	{
		typedef VMatrix& (__thiscall* oW2SM)(PVOID);
		return CallVirtual< oW2SM >(this, 37)(this);
	}

	void ExecuteClientCmd(const char *szCmdString)
	{
		typedef void(__thiscall* oExecClientCmd)(PVOID, const char*);
		return CallVirtual< oExecClientCmd >(this, 105)(this, szCmdString);
	}
};

class IPanel
{
public:

	const char *GetName(unsigned int vguiPanel)
	{
		typedef const char* (__thiscall* oGetName)(PVOID, unsigned int);
		return CallVirtual< oGetName >(this, 36)(this, vguiPanel);
	}
};

class ISurface
{
public:

	void DrawSetColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* oDrawSetColor)(PVOID, int, int, int, int);
		return CallVirtual< oDrawSetColor >(this, CSGO_VGUIMATSURFACE_DrawSetColor)(this, r, g, b, a);
	}

	void DrawFilledRect(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* oDrawFilledRect)(PVOID, int x0, int y0, int x1, int y1);
		return CallVirtual< oDrawFilledRect >(this, CSGO_VGUIMATSURFACE_DrawFilledRect)(this, x0, y0, x1, y1);
	}

	void DrawOutlinedRect(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* oDrawOutlinedRect)(PVOID, int, int, int, int);
		return CallVirtual< oDrawOutlinedRect >(this, CSGO_VGUIMATSURFACE_DrawOutlinedRect)(this, x0, y0, x1, y1);
	}

	void DrawLine(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* oDrawLine)(PVOID, int x0, int y0, int x1, int y1);
		return CallVirtual< oDrawLine >(this, CSGO_VGUIMATSURFACE_DrawLine)(this, x0, y0, x1, y1);
	}

	void DrawPolyLine(int *px, int *py, int numPoints)
	{
		typedef void(__thiscall* oDrawPolyLine)(PVOID, int*, int*, int);
		return CallVirtual< oDrawPolyLine >(this, CSGO_VGUIMATSURFACE_DrawPolyLine)(this, px, py, numPoints);
	}

	void DrawSetTextFont(unsigned long font)
	{
		typedef void(__thiscall* oDrawSetTextFont)(PVOID, unsigned long font);
		return CallVirtual< oDrawSetTextFont >(this, CSGO_VGUIMATSURFACE_DrawSetTextFont)(this, font);
	}

	void DrawSetTextColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* oDrawSetTextColor)(PVOID, int r, int g, int b, int a);
		return CallVirtual< oDrawSetTextColor >(this, CSGO_VGUIMATSURFACE_DrawSetTextColor)(this, r, g, b, a);
	}

	void DrawSetTextPos(int x, int y)
	{
		typedef void(__thiscall* oDrawSetTextPos)(PVOID, int x, int y);
		return CallVirtual< oDrawSetTextPos >(this, CSGO_VGUIMATSURFACE_DrawSetTextpos)(this, x, y);
	}

	void DrawPrintText(const wchar_t *text, int textLen)
	{
		typedef void(__thiscall* oDrawPrintText)(PVOID, const wchar_t *text, int textLen, int drawType);
		return CallVirtual< oDrawPrintText >(this, CSGO_VGUIMATSURFACE_DrawPrintText)(this, text, textLen, 0);
	}

	unsigned long CreateFont()
	{
		typedef unsigned long(__thiscall* oCreateFont)(PVOID);
		return CallVirtual< oCreateFont >(this, CSGO_VGUIMATSURFACE_CreateFont)(this);
	}

	bool SetFontGlyphSet(unsigned long font, const char *windowsFontName, int tall, int weight, int blur, int scanlines, int flags)
	{
		typedef bool(__thiscall* oSetFontGlyphSet)(PVOID, unsigned long font, const char *windowsFontName, int tall, int weight, int blur, int scanlines, int flags, int nRangeMin, int nRangeMax);
		return CallVirtual< oSetFontGlyphSet >(this, CSGO_VGUIMATSURFACE_SetFontGlyphSet)(this, font, windowsFontName, tall, weight, blur, scanlines, flags, 0, 0);
	}

	/*void GetTextSize(unsigned long font, const wchar_t *text, int &wide, int &tall)
	{
	typedef void(__thiscall* oGetTextSize)(PVOID, unsigned long font, const wchar_t *text, int &wide, int &tall);
	return CallVirtual< oGetTextSize >(this, 79)(this, font, text, wide, tall);
	}*/

	bool GetTextSize(int Font, const wchar_t* _Input, int& Wide, int& Tall)
	{
		typedef bool(__thiscall* _GetTextSize)(void*, int, const wchar_t*, int&, int&);
		return CallVirtual< _GetTextSize >(this, CSGO_VGUIMATSURFACE_GetTextSize)(this, Font, _Input, Wide, Tall);
	}

	/*void DrawColoredText(unsigned long font, int x, int y, int r, int g, int b, int a, char *fmt, ...)
	{
	typedef void(__thiscall* oDrawTxt)(PVOID, unsigned long, int, int, int, int, int, int, char*);
	return CallVirtual< oDrawTxt >(this, 162)(this, font, x, y, r, g, b, a, fmt);
	}*/
};

class IInputSystem
{
public:

	void GetCursorPosition( int* m_pX, int* m_pY )
	{
		typedef void( __thiscall* OriginalFn )( void*, int*, int* );
		return CallVirtual<OriginalFn>( this, 56 )( this, m_pX, m_pY );
	}
};