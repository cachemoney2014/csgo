//==================================================================================================================================
#include "Required.h"
#include "Memory.h"
//==================================================================================================================================
#include "Cvars.h"
#include "Drawing.h"
#include "GUI.h"
#include "Hook.h"
#include "Init.h"
#include "Rendering.h"
//==================================================================================================================================
typedef BOOL ( WINAPI* GetCursorPos_t )( POINT* lpPoint );
//==================================================================================================================================
typedef bool ( __stdcall* PaintTravese_t )( uintptr_t vguiPanel, bool forceRepaint, bool allowForce );
//==================================================================================================================================
typedef SHORT ( WINAPI* GetAsyncKeyState_t )( int vKey );
//==================================================================================================================================
void HackPaintTraverse ( uintptr_t vguiPanel )
{
	int X, Y;

	const char* Name;

	if ( g_GUI.GetActive() )
	{		
		g_EncryptedInputSystem.Get<IInputSystem>()->GetCursorPosition ( &X, &Y );

		g_GUI.SetMouseXY ( X, Y );

		Name = g_EncryptedPanel.Get<IPanel>()->GetName ( vguiPanel );

		if ( Name[0] == 'M' && Name[3] == 'S' )
		{
			g_GUI.m_Clicked = g_EncryptedGetAsyncKeyState.Get2<GetAsyncKeyState_t>()( 0x01 ) & 1;

			g_GUI.DrawGUI();
			g_GUI.DrawMouse();
		}
		
	}
}
//==================================================================================================================================
void __stdcall PaintTraverse_Hook ( uintptr_t vguiPanel, bool forceRepaint, bool allowForce )
{
	VGUIMatSurfaceVGUIPanel009.GetOriginalVTableMethod<PaintTravese_t>(41)( vguiPanel, forceRepaint, allowForce );

	HackPaintTraverse ( vguiPanel );
}
//==================================================================================================================================