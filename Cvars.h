//==================================================================================================================================
#pragma once
//==================================================================================================================================
#define FLAG_NONE	( 0 << 0 )
#define FLAG_CHECKBOX ( 1 << 0 )
#define FLAG_SLIDER ( 1 << 1 )
#define FLAG_KEYBOX ( 1 << 2 )
#define FLAG_MULTIBOX ( 1 << 3 )
//==================================================================================================================================
struct Cvar_s
{	
	char* m_MenuString;
	char* m_IniString;
	int m_Value;
	int m_MinValue;
	int m_MaxValue;
	int m_Flags;
	int m_Step;
	
	int m_LengthMenuString;
	int m_KeyMenuString;

	int m_LengthIniString;
	int m_KeyIniString;

	bool m_Waiting;
};
//==================================================================================================================================
struct Cvar_Group
{
	Cvar_s** m_Cvars;
	char* m_Name;

	int m_Key;
	int m_Length;
};
//==================================================================================================================================
struct Cvar_Parent
{
	Cvar_Group* m_Group;

	char* m_Name;

	int m_Key;
	int m_Length;
};
//==================================================================================================================================
namespace cvars
{
	// legit

	extern Cvar_s aim_legit_aimbot;
	extern Cvar_s aim_legit_autoshoot;
	extern Cvar_s aim_legit_friendlyfire;
	extern Cvar_s aim_legit_speed;
	
	extern Cvar_s trig_legit_triggerbot;
	extern Cvar_s trig_legit_triggerbot_key;
	extern Cvar_s trig_legit_triggerbot_delay;
	

	extern Cvar_s trig_legit_spot_head;// = { LegitHead, TriggerSpotHead, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_neck;// = { LegitNeck, TriggerSpotNeck, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_highspine;// = { LegitHighSpine, TriggerSpotHighSpine, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_lowspine;// = { LegitLowSpine, TriggerSpotLowSpine, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_pelvis;// = { LegitPelvis, TriggerSpotPelvis, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_thigh;// = { LegitThigh, TriggerSpotThigh, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_upperarm;// = { LegitUpperArm, TriggerSpotUpperArm, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_forearm;// = { LegitForeArm, TriggerSpotForeArm, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s trig_legit_spot_calf;// = { LegitCalf, TriggerSpotCalf, 0, 0, 1, FLAG_CHECKBOX };
	
	extern Cvar_s aim_legit_pistol_fov;// = { LegitPistolFOV, AimLegitPistolFOV, 0, 0, 180, FLAG_SLIDER };
	extern Cvar_s aim_legit_pistol_recoil;// = { LegitPistolRecoil, AimLegitPistolRecoil, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s aim_legit_pistol_hitcapsule;// = { LegitPistolHitcapsule, AimLegitPistolHitcapsule, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s aim_legit_pistol_autowall;// = { LegitPistolAutowall, AimLegitPistolAutowall, 0, 0, 1, FLAG_CHECKBOX };
	
	extern Cvar_s aim_legit_rifle_fov;// = { LegitRifleFOV, AimLegitRifleFOV, 0, 0, 180, FLAG_SLIDER };
	extern Cvar_s aim_legit_rifle_recoil;// = { LegitRifleRecoil, AimLegitRifleRecoil, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s aim_legit_rifle_hitcapsule;// = { LegitRifleHitcapsule, AimLegitRifleHitcapsule, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s aim_legit_rifle_autowall;// = { LegitRifleAutowall, AimLegitRifleAutowall, 0, 0, 1, FLAG_CHECKBOX };
	
	extern Cvar_s aim_legit_sniper_fov;// = { LegitSniperFOV, AimLegitSniperFOV, 0, 0, 180, FLAG_SLIDER };
	extern Cvar_s aim_legit_sniper_recoil;// = { LegitSniperRecoil, AimLegitSniperRecoil, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s aim_legit_sniper_hitcapsule;// = { LegitSniperHitcapsule, AimLegitSniperHitcapsule, 0, 0, 1, FLAG_CHECKBOX };
	extern Cvar_s aim_legit_sniper_autowall;// = { LegitSniperAutowall, AimLegitSniperAutowall, 0, 0, 1, FLAG_CHECKBOX };

	// rage

	extern Cvar_s aim_rage_aimbot;
	extern Cvar_s aim_rage_autoshoot;
	extern Cvar_s aim_rage_friendlyfire;
	extern Cvar_s aim_rage_noviewchange;
	extern Cvar_s aim_rage_bodyaim;
	extern Cvar_s aim_rage_lock;
	extern Cvar_s aim_rage_mode;
	
	extern Cvar_s accuracy_rage_norecoil;
	extern Cvar_s accuracy_rage_nospread;
	extern Cvar_s accuracy_rage_autowall;
	extern Cvar_s accuracy_rage_positionadjustment;

	extern Cvar_s aim_spot_head;
	extern Cvar_s aim_spot_neck;
	extern Cvar_s aim_spot_highspine;
	extern Cvar_s aim_spot_lowspine;
	extern Cvar_s aim_spot_pelvis;
	extern Cvar_s aim_spot_thigh;
	extern Cvar_s aim_spot_upperarm;;
	extern Cvar_s aim_spot_forearm;
	extern Cvar_s aim_spot_calf;


	extern Cvar_s visual_esp;

	extern Cvar_Parent Rage_Tab[];
	extern Cvar_Parent Legit_Tab[];
	extern Cvar_Parent Misc_Tab[];
	extern Cvar_Parent Visuals_Tab[];
	extern Cvar_Parent Colors_Tab[];

};
//==================================================================================================================================

//==================================================================================================================================