//==================================================================================================================================
typedef struct
{
	bool m_IsAlive;

	float m_Distance;

	float m_FOV;

	float m_FOVSqr;
	float m_FOVHorizontal;
	float m_FOVVertical;

	float m_SimulationTime;
	float m_LastSimulationTime;

	float m_EyePosition[3];
	float m_Origin[3];
	float m_Angles[3];
	float m_LastAngles[3];
	float m_PunchAngles[3];
	float m_Velocity[3];

	float m_Forward[3];
	float m_Right[3];
	float m_Up[3];

	float m_Ping;

	uintptr_t m_CommandsQueued;
	uintptr_t m_Index;
	uintptr_t m_TickBase;
	uintptr_t m_TickCount;
	
	uintptr_t m_Flags;
	uintptr_t m_Seed;
	uintptr_t m_WeaponID;

}CSGO_Player_Data_t;
//==================================================================================================================================
typedef struct
{
	uintptr_t m_PlayerCount;

	CSGO_Player_Data_t m_Data[ 64 ];
}CSGO_Player_t;
//==================================================================================================================================
typedef struct
{
	CSGO_Player_Data_t m_Data;
}CSGO_LocalPlayer_t;
//==================================================================================================================================
extern void __stdcall ClientMode_OverrideMouseInput_Hook ( float *x, float *y );
extern bool __stdcall ClientMode_CreateMove_Hook ( float flInputSampleTime, CUserCmd* pCmd );
extern int __stdcall ClientMode_HudElementKeyInput_Hook ( int down, ButtonCode_t keynum, const char *pszCurrentBinding );
//==================================================================================================================================
extern int __stdcall IN_KeyEvent_Hook ( int eventcode, ButtonCode_t keynum, const char *pszCurrentBinding );
extern void __stdcall CreateMove_Hook ( int sequence_number, float input_sample_frametime, bool active );
extern void SetViewAngles_Hook ( Vector& va );
//==================================================================================================================================
extern CSGO_LocalPlayer_t CSGO_LocalPlayer;
extern CSGO_Player_t CSGO_Player;
//==================================================================================================================================