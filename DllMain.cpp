//==================================================================================================================================
#include "Required.h"
#include "Memory.h"
#include "Hook.h"
#include "Init.h"

static bool bDone = false;

// todo: DLLMain can be wiped since we're manually mapping the whole dll, and it will not be receiving any DLL_PROCESS_ATTACH, or anything of that nature

//==================================================================================================================================
BOOL WINAPI DLLMain ( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
	switch ( fdwReason )
	{
		case DLL_PROCESS_ATTACH:
		{			
			DLLStartup ( ( uintptr_t )hinstDLL, lpvReserved );
		
//			Setup();
		}
		break;
	}

	return TRUE;
}
//==================================================================================================================================