//==================================================================================================================================
#include "Required.h"
#include "Memory.h"
//==================================================================================================================================
#include "Cvars.h"
#include "Drawing.h"
#include "GUI.h"
#include "Hook.h"
#include "Init.h"
//==================================================================================================================================
CGUI g_GUI;
//==================================================================================================================================
typedef SHORT ( WINAPI* GetAsyncKeyState_t )( int vKey );
typedef BOOL ( WINAPI* GetKeyboardState_t )( PBYTE lpKeyState );
typedef int ( WINAPI* GetKeyNameText_t )( LONG lParam, LPTSTR lpString, int cchSize );
typedef int ( WINAPI* MapVirtualKey_t )( UINT uCode, UINT uMapType );
//==================================================================================================================================
int itoa(int value, char *sp, int radix)
{
    char tmp[16];// be careful with the length of the buffer
    char *tp = tmp;
    int i;
    unsigned v;

    int sign = (radix == 10 && value < 0);    
    if (sign)
        v = -value;
    else
        v = (unsigned)value;

    while (v || tp == tmp)
    {
        i = v % radix;
        v /= radix; // v/=radix uses less CPU clocks than v=v/radix does
        if (i < 10)
          *tp++ = i+'0';
        else
          *tp++ = i + 'a' - 10;
    }

    int len = tp - tmp;

    if (sign) 
    {
        *sp++ = '-';
        len++;
    }

    while (tp > tmp)
        *sp++ = *--tp;

    return len;
}
//==================================================================================================================================
void CGUI::SetXY ( int X, int Y )
{
	m_X = X;
	m_Y = Y;
}
//==================================================================================================================================
void CGUI::SetMouseXY ( int X, int Y )
{	
	m_MouseX = X;
	m_MouseY = Y;
			
	if ( m_MouseX < g_0 )
	{
		m_MouseX = g_0;
	}
	else if ( m_MouseX > m_ScreenSizeX )
	{
		m_MouseX = m_ScreenSizeX;
	}

	if ( m_MouseY < g_0 )
	{
		m_MouseY = g_0;
	}
	else if ( m_MouseY > m_ScreenSizeY )
	{
		m_MouseY = m_ScreenSizeY;
	}
}
//==================================================================================================================================
void CGUI::SetWidthAndHeight ( int Width, int Height )
{
	m_Width = Width;
	m_Height = Height;
}
//==================================================================================================================================
bool CGUI::CheckForClick ( int X, int Y, int W, int H )
{
	if ( m_MouseX >= X && m_MouseX <= ( X + W )
		&& m_MouseY >= Y && m_MouseY <= ( Y + H )
		&& m_Clicked == true )
	{
		return true;
	}

	return false;
}
//==================================================================================================================================
bool CGUI::CheckForClick2 ( int X, int Y, int W, int H )
{
	if ( m_MouseX >= X && m_MouseX <= ( X + W )
		&& m_MouseY >= Y && m_MouseY <= ( Y + H )
		&& g_EncryptedGetAsyncKeyState.Get2<GetAsyncKeyState_t>()(0x01) )
	{
		return true;
	}

	return false;
}
//==================================================================================================================================
void CGUI::DrawMouse ( void )
{
	Color_t Black;
	Color_t White;

	White.Init ( 255, 255, 255, 255 );
	Black.Init ( 0, 0, 0, 255 );

	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 1, 1, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 2, 2, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 3, 3, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 4, 4, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 5, 5, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 6, 6, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 7, 7, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 8, 8, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 9, 9, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 10, 10, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 11, 11, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 12, 12, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 13, 13, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 14, 14, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 15, 15, 1, White );
	g_Draw.DrawFilledRect ( m_MouseX, m_MouseY + 16, 16, 1, White );

	g_Draw.DrawLine ( m_MouseX, m_MouseY, m_MouseX, m_MouseY + 16, Black );
	g_Draw.DrawLine ( m_MouseX, m_MouseY, m_MouseX + 16, m_MouseY + 16, Black );
	g_Draw.DrawLine ( m_MouseX, m_MouseY + 16, m_MouseX + 16, m_MouseY + 16, Black );
}

//==================================================================================================================================
void CGUI::DrawGUI ( void )
{	
	char Number[8];

	char KeyName[256];

	int X, Y, BoxGroupX, BoxGroupY, CvarX, CvarY, GroupHeight, Width, Height;

	Color_t DarkGrey ( 63, 63, 63, 255 );
	Color_t Grey ( 127, 127, 127, 255 );
	Color_t Black ( 0, 0, 0, 255 );
	Color_t White ( 255, 255, 255, 255 );
	Color_t Red ( 255, 0, 0, 255 );

	Cvar_Parent* pParent;
	Cvar_Group* pGroup;
	Cvar_s** ppCvar;
	Cvar_s* pCvar;

	if ( m_Picked == true )
	{
		g_Draw.DrawFilledRect ( m_X, m_Y, m_Width, m_Height, Grey );
	}
	else
	{
		g_Draw.DrawFilledRect ( m_X, m_Y, m_Width, TOPBAR_HEIGHT, Grey );
	}

	g_Draw.GradientV ( m_X + 1, m_Y + 1, m_Width - 1, TOPBAR_HEIGHT, DarkGrey, Grey );
		
//	g_Draw.DrawString ( m_X + m_Width / 2, MENU_FONT_SIZE, White, "GUWOPWare", AlignCenterH | AlignCenterV, g_Draw.m_MenuFont );
		
	X = m_X + 1;
	Y = m_Y + TOPBAR_HEIGHT;

	g_Draw.GradientV ( X, Y, m_Width - 1, TAB_HEIGHT, Grey, Black );
	
	for ( size_t Iterator = 0; Iterator < MAX_TABS; Iterator++ )
	{		
		if ( CheckForClick ( X + Iterator * TAB_WIDTH, Y, TAB_WIDTH, TAB_HEIGHT ) )
		{
			m_Tabs[ m_LastTab ].m_Active = false;
						
			m_Tabs[ Iterator ].m_Active = true;

			m_IsWaiting = false;

			m_Picked = true;

			g_Draw.DrawOutlinedRect ( m_X, m_Y, m_Width, m_Height, Black );

			m_LastTab = Iterator;
		}

		if ( m_Tabs [ Iterator ].m_Active == true )
		{
			g_Draw.GradientV ( X + Iterator * TAB_WIDTH, Y, TAB_WIDTH, TAB_HEIGHT, Red, Black );
		
			pParent = m_Tabs [ Iterator ].Parent;

			pGroup = pParent->m_Group;
					
			BoxGroupX = X + TAB_WIDTH / 4;
			BoxGroupY = Y + TAB_HEIGHT * 2;
/*								
			for ( ; pGroup->m_Name != 0; pGroup++ )
			{
				
				g_Draw.DrawString ( BoxGroupX, BoxGroupY, White, pGroup->m_Name, AlignCenterH | AlignCenterV, g_Draw.m_MenuFont );

				CvarX = BoxGroupX;
				CvarY = BoxGroupY;

				ppCvar = pGroup->m_Cvars;

				CvarX += 24;

				for ( ; *ppCvar; ppCvar++ )
				{
					CvarY += 24;

					pCvar = *ppCvar;
						
					g_Draw.DrawString ( CvarX, CvarY, White, pCvar->m_MenuString, AlignCenterH | AlignCenterV, g_Draw.m_MenuFont );
				}
				
				BoxGroupX += TAB_WIDTH;
			}
*/
			
			for ( size_t Row = 0; pGroup->m_Name != 0; pGroup++ )
			{
				GroupHeight = 0;

				ppCvar = pGroup->m_Cvars;

				for ( ; *ppCvar; ppCvar++ )
				{
					pCvar = *ppCvar;

					GroupHeight += MENU_FONT_SIZE + 15;
				}
				
				g_Draw.DrawOutlinedRect ( BoxGroupX, BoxGroupY, GROUP_WIDTH, GroupHeight, Black );
				g_Draw.DrawOutlinedRect ( BoxGroupX-1, BoxGroupY-1, GROUP_WIDTH+3, GroupHeight+3, White );
				g_Draw.DrawOutlinedRect ( BoxGroupX, BoxGroupY, GROUP_WIDTH, GroupHeight, Black );
				g_Draw.DrawFilledRect ( BoxGroupX+1, BoxGroupY+1, GROUP_WIDTH, GroupHeight, DarkGrey );

				ppCvar = pGroup->m_Cvars;

				g_Draw.DrawStringEncrypted ( BoxGroupX + ( GROUP_WIDTH / 2 ), BoxGroupY - MENU_FONT_SIZE, White, pGroup->m_Name, pGroup->m_Length, pGroup->m_Key, Width, Height,  AlignCenterH | AlignCenterV, g_Draw.m_MenuFont );

				CvarX = BoxGroupX;
				CvarY = BoxGroupY + 12;

				for ( ; *ppCvar; ppCvar++ )
				{
					pCvar = *ppCvar;

					g_Draw.DrawStringEncrypted ( CvarX + 12, CvarY, White, pCvar->m_MenuString, pCvar->m_LengthMenuString, pCvar->m_KeyMenuString, Width, Height, 0, g_Draw.m_MenuFont );

					if ( pCvar->m_Flags & FLAG_CHECKBOX )
					{
						g_Draw.DrawOutlinedRect ( CvarX + 110, CvarY, 15, 15, White );
						g_Draw.DrawOutlinedRect ( CvarX + 109, CvarY-1, 15, 15, Black );

						if ( CheckForClick ( CvarX + 110, CvarY, 15, 15 ) )
						{
							pCvar->m_Value = !pCvar->m_Value;
						}

						if ( pCvar->m_Value )
						{
							g_Draw.DrawFilledRect ( CvarX + 110 + 2, CvarY + 2, 11, 11, Red );
						}
					}
					else if ( pCvar->m_Flags & FLAG_SLIDER )
					{
						g_Draw.DrawOutlinedRect ( CvarX + 110, CvarY, 90, 15, White );
						g_Draw.DrawOutlinedRect ( CvarX + 109, CvarY-1, 90, 15, Black );

						float Step, Value = 0;
												
						Step = g_Draw.GetSliderStep ( pCvar->m_MaxValue, 90 );
												
						if ( CheckForClick2 ( CvarX + 110, CvarY, 90, 15 ) )
						{	
							if ( pCvar->m_MaxValue < 90 )
							{
								Value = ( ( float )m_MouseX - ( float )( CvarX + 110 ) ) / Step;
							}
							else
							{
								Value = ( ( float )m_MouseX - ( float )( CvarX + 110 ) ) * Step;
							}

							pCvar->m_Value = Value;
						}

						if ( pCvar->m_Value > pCvar->m_MaxValue )
						{
							pCvar->m_Value = pCvar->m_MaxValue;
						}

						if ( pCvar->m_MaxValue < 90 )
						{
							g_Draw.DrawFilledRect ( CvarX + 109 + ( pCvar->m_Value * Step ), CvarY + 1, 3, 14, Red );
						}
						else
						{
							g_Draw.DrawFilledRect ( CvarX + 109 + ( pCvar->m_Value / Step ), CvarY + 1, 3, 14, Red );
						}

						memset ( &Number, 0, sizeof ( Number ) );

						itoa ( pCvar->m_Value, Number, 10 );

						g_Draw.DrawString ( CvarX + 110 + 45, CvarY + 20, White, Number, AlignCenterH | AlignCenterV, g_Draw.m_MenuFont );
					}
					else if ( pCvar->m_Flags & FLAG_KEYBOX )
					{
						g_Draw.DrawOutlinedRect ( CvarX + 110, CvarY, 90, 15, White );
						g_Draw.DrawOutlinedRect ( CvarX + 109, CvarY-1, 90, 15, Black );
						
						if ( CheckForClick2 ( CvarX + 110, CvarY, 90, 15 ) )
						{
							pCvar->m_Waiting = true;
						}

						if ( pCvar->m_Waiting == true )
						{
							if ( m_Counter > 100 )
							{
								for ( size_t Iterator = 0; Iterator < 256; Iterator++ )
								{
									if ( g_EncryptedGetAsyncKeyState.Get2<GetAsyncKeyState_t>()(Iterator) )
									{
										pCvar->m_Value = Iterator;

										m_Counter = 0;

										pCvar->m_Waiting = false;

										break;
									}
								}
							}
							
							m_Counter++;
						}
						
						if ( pCvar->m_Waiting == false )
						{
							int ScanCode = g_MapVirtualKey.Get2<MapVirtualKey_t>()( pCvar->m_Value, 0 );
							
							switch ( pCvar->m_Value )
							{
								case VK_LEFT: case VK_UP: case VK_RIGHT: case VK_DOWN:
								case VK_PRIOR: case VK_NEXT:
								case VK_END: case VK_HOME:
								case VK_INSERT: case VK_DELETE:
								case VK_DIVIDE:
								case VK_NUMLOCK:
								{
									ScanCode |= 0x100;
									break;
								}
							}
							
							if ( g_GetKeyNameText.Get2<GetKeyNameText_t>()( ScanCode << 16, KeyName, sizeof ( KeyName ) ) )
							{
								g_Draw.DrawString ( CvarX + 110 + 45, CvarY + 7, White, KeyName, AlignCenterH | AlignCenterV, g_Draw.m_MenuFont );
							}
						}

						memset ( &KeyName, 0, sizeof ( KeyName ) );
					}

					CvarY += 24;
				}

				BoxGroupX += GROUP_WIDTH + 50;

				Row++;

				if ( Row >= 3 )
				{
					Row = 0;

					BoxGroupX = X + TAB_WIDTH / 4;

					BoxGroupY += 275;
				}
			}
		}

		g_Draw.DrawStringEncrypted ( X + Iterator * TAB_WIDTH + TAB_WIDTH / 2, TAB_HEIGHT, White, m_Tabs[ Iterator ].Parent->m_Name, m_Tabs[ Iterator ].Parent->m_Length, m_Tabs[ Iterator ].Parent->m_Key, Width, Height, AlignCenterH | AlignCenterV, g_Draw.m_MenuFont );
	}
		
	g_Draw.DrawOutlinedRect ( m_X, m_Y, m_Width, TOPBAR_HEIGHT, Black );
	
	if ( g_GUI.m_Clicked )
	{
		g_GUI.m_Clicked = false;
	}
}
//==================================================================================================================================