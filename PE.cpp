//=============================================================================================================
#include "PE.h"
//========================================================================================
extern "C" 
{
	int _fltused = 1;
}
//==================================================================================================================================
#include "wipe.h"
#include "wipe_start.h"
//==================================================================================================================================
PROCESS_ENVIRONMENT_BLOCK* GetPEB ( void )
{
	PROCESS_ENVIRONMENT_BLOCK* PEB;

	__asm
	{
		PUSH EAX
		MOV EAX, DWORD PTR FS:[ 0x30 ]
		MOV PEB, EAX
		POP EAX
	}

	return PEB;
}
//==================================================================================================================================
#include "wipe_end.h"
//=============================================================================================================