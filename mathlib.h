//==================================================================================================================================
#pragma once
//==================================================================================================================================
class QAngle;
class Vector;
//==================================================================================================================================
typedef float vec_t;
//==================================================================================================================================
// 3x4 matrix (used in Source SDK)
struct matrix3x4_t
{
	matrix3x4_t ( void ) {}
	matrix3x4_t (
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23 )
	{
		m_flMatVal[ 0 ][ 0 ] = m00; m_flMatVal[ 0 ][ 1 ] = m01; m_flMatVal[ 0 ][ 2 ] = m02; m_flMatVal[ 0 ][ 3 ] = m03;
		m_flMatVal[ 1 ][ 0 ] = m10; m_flMatVal[ 1 ][ 1 ] = m11; m_flMatVal[ 1 ][ 2 ] = m12; m_flMatVal[ 1 ][ 3 ] = m13;
		m_flMatVal[ 2 ][ 0 ] = m20; m_flMatVal[ 2 ][ 1 ] = m21; m_flMatVal[ 2 ][ 2 ] = m22; m_flMatVal[ 2 ][ 3 ] = m23;
	}

	inline float *operator[] ( int i ) { return m_flMatVal[ i ]; }
	inline float const *operator[] ( int i ) const { return m_flMatVal[ i ]; }

	inline float *Base ( void ) { return &m_flMatVal[ 0 ][ 0 ]; }
	inline float const *Base ( void ) const { return &m_flMatVal[ 0 ][ 0 ]; }

	float m_flMatVal[ 3 ][ 4 ];
};
//==================================================================================================================================
// 4x4 matrix (used in Source SDK)
struct VMatrix
{
	VMatrix ( void ) {}
	VMatrix (
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33 )
	{
		Init (
			m00, m01, m02, m03,
			m10, m11, m12, m13,
			m20, m21, m22, m23,
			m30, m31, m32, m33 );
	}

	void Init (
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33 )
	{
		m[ 0 ][ 0 ] = m00; m[ 0 ][ 1 ] = m01; m[ 0 ][ 2 ] = m02; m[ 0 ][ 3 ] = m03;
		m[ 1 ][ 0 ] = m10; m[ 1 ][ 1 ] = m11; m[ 1 ][ 2 ] = m12; m[ 1 ][ 3 ] = m13;
		m[ 2 ][ 0 ] = m20; m[ 2 ][ 1 ] = m21; m[ 2 ][ 2 ] = m22; m[ 2 ][ 3 ] = m23;
		m[ 3 ][ 0 ] = m30; m[ 3 ][ 1 ] = m31; m[ 3 ][ 2 ] = m32; m[ 3 ][ 3 ] = m33;
	}

	inline float *operator[] ( int i ) { return m[ i ]; }
	inline float const *operator[] ( int i ) const { return m[ i ]; }

	inline float *Base ( void ) { return &m[ 0 ][ 0 ]; }
	inline float const *Base ( void ) const { return &m[ 0 ][ 0 ]; }

	float m[ 4 ][ 4 ];
};
//==================================================================================================================================
inline bool IsFinite( vec_t f )
{
	return ( ( ( uintptr_t )f & 0x7F800000) != 0x7F800000 );
}
//==================================================================================================================================
float _acos ( float Cosine );
//==================================================================================================================================
float _atan ( float Tangent );
//==================================================================================================================================
float _atan2 ( float Sine, float Cosine );
//==================================================================================================================================
float _sqrt ( float AngleInRadians );
//==================================================================================================================================
float _cos ( float AngleInRadians );
//==================================================================================================================================
float _sin ( float AngleInRadians );
//==================================================================================================================================
float Get1();
//==================================================================================================================================