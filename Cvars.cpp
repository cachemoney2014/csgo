//==================================================================================================================================
#include "Required.h"
#include "Cvars.h"
//==================================================================================================================================
#include "Colors.h"
#include "Legit.h"
#include "Misc.h"
#include "Rage.h"
#include "Visuals.h"
//==================================================================================================================================
char g_LegitTabName[] = "Legit";
char g_RageTabName[] = "Rage";
char g_VisualsTabName[] = "Visuals";
char g_MiscTabName[] = "Misc";
char g_ColorsTabName[] = "Colors";
//==================================================================================================================================
namespace cvars
{	
	// legit

	Cvar_s aim_legit_aimbot = { EnableLegitAimbot, AimLegitAimbot, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_aimkey = { LegitAimKey, AimLegitAimKey, 0, 0, 1, FLAG_KEYBOX };
	Cvar_s aim_legit_autoshoot = { LegitAutoShoot, AimLegitAutoShoot, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_friendlyfire = { LegitFriendlyFire, AimLegitFriendlyFire, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_speed = { LegitSpeedLimit, AimLegitSpeed, 0, 0, 180, FLAG_SLIDER, 5 };

	Cvar_s trig_legit_triggerbot = { EnableTriggerbot, AimTriggerbot, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_triggerbot_key = { TriggerbotKey, AimTriggerbotKey, 0, 0, 256, FLAG_KEYBOX };
	Cvar_s trig_legit_triggerbot_delay = { TriggerbotDelay, AimTriggerbotDelay, 0, 0, 15, FLAG_SLIDER, 1 };
	Cvar_s trig_legit_triggerbot_hitchance = { TriggerbotHitchance, AimTriggerbotHitchance, 0, 0, 100, FLAG_SLIDER };

	Cvar_s aim_legit_pistol_fov = { LegitPistolFOV, AimLegitPistolFOV, 0, 0, 180, FLAG_SLIDER };
	Cvar_s aim_legit_pistol_recoil = { LegitPistolRecoil, AimLegitPistolRecoil, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_pistol_hitcapsule = { LegitPistolHitcapsule, AimLegitPistolHitcapsule, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_pistol_autowall = { LegitPistolAutowall, AimLegitPistolAutowall, 0, 0, 1, FLAG_CHECKBOX };
	
	Cvar_s aim_legit_rifle_fov = { LegitRifleFOV, AimLegitRifleFOV, 0, 0, 180, FLAG_SLIDER };
	Cvar_s aim_legit_rifle_recoil = { LegitRifleRecoil, AimLegitRifleRecoil, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_rifle_hitcapsule = { LegitRifleHitcapsule, AimLegitRifleHitcapsule, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_rifle_autowall = { LegitRifleAutowall, AimLegitRifleAutowall, 0, 0, 1, FLAG_CHECKBOX };
	
	Cvar_s aim_legit_sniper_fov = { LegitSniperFOV, AimLegitSniperFOV, 0, 0, 180, FLAG_SLIDER };
	Cvar_s aim_legit_sniper_recoil = { LegitSniperRecoil, AimLegitSniperRecoil, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_sniper_hitcapsule = { LegitSniperHitcapsule, AimLegitSniperHitcapsule, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_legit_sniper_autowall = { LegitSniperAutowall, AimLegitSniperAutowall, 0, 0, 1, FLAG_CHECKBOX };
	
	Cvar_s trig_legit_spot_head = { LegitHead, TriggerSpotHead, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_neck = { LegitNeck, TriggerSpotNeck, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_highspine = { LegitHighSpine, TriggerSpotHighSpine, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_lowspine = { LegitLowSpine, TriggerSpotLowSpine, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_pelvis = { LegitPelvis, TriggerSpotPelvis, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_thigh = { LegitThigh, TriggerSpotThigh, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_upperarm = { LegitUpperArm, TriggerSpotUpperArm, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_forearm = { LegitForeArm, TriggerSpotForeArm, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s trig_legit_spot_calf = { LegitCalf, TriggerSpotCalf, 0, 0, 1, FLAG_CHECKBOX };

	// rage

	Cvar_s aim_rage_aimbot = { EnableRageAimbot, AimRageAimbot, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_autoshoot = { RageAutoShoot, AimRageAutoShoot, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_noviewchange = { RageNoViewChange, AimRageNoViewChange, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_bodyaim = { RageBodyAim, AimRageBodyAim, 0, 0, 1, FLAG_CHECKBOX };
	
	Cvar_s accuracy_rage_norecoil = { NoRecoil, AccuracyNoRecoil, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s accuracy_rage_nospread = { NoSpread, AccuracyNoSpread, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s accuracy_rage_autowall = { AutoWall, AccuracyAutoWall, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s accuracy_rage_positionadjustment = { PositionAdjustment, AccuracyPositionAdjustment, 0, 0, 1, FLAG_CHECKBOX };

	Cvar_s aim_rage_lock = { RageAimLockOnTarget, AimRageLock, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_mode = { RageTargetMode, AimRageMode, 0, 0, 4, FLAG_MULTIBOX };
	Cvar_s aim_rage_friendlyfire = { RageFriendlyFire, AimRageFriendlyFire, 0, 0, 1, FLAG_CHECKBOX };
		
	Cvar_s aim_rage_spot_head = { RageHead, AimSpotHead, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_neck = { RageNeck, AimSpotNeck, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_highspine = { RageHighSpine, AimSpotHighSpine, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_lowspine = { RageLowSpine, AimSpotLowSpine, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_pelvis = { RagePelvis, AimSpotPelvis, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_thigh = { RageThigh, AimSpotThigh, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_upperarm = { RageUpperArm, AimSpotUpperArm, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_forearm = { RageForeArm, AimSpotForeArm, 0, 0, 1, FLAG_CHECKBOX };
	Cvar_s aim_rage_spot_calf = { RageCalf, AimSpotCalf, 0, 0, 1, FLAG_CHECKBOX };


	Cvar_s visual_esp = { EnableVisualEsp, VisualEsp, 0, 0, 1, FLAG_CHECKBOX };
	
	Cvar_s* Legit_Aimbot_List[] = 
	{
		&aim_legit_aimbot,
		&aim_legit_aimkey,
		&aim_legit_autoshoot,
		&aim_legit_friendlyfire,
		&aim_legit_speed,
		0
	};

	Cvar_s* Legit_Triggerbot_List[] = 
	{
		&trig_legit_triggerbot,
		&trig_legit_triggerbot_key,
		&trig_legit_triggerbot_delay,
		&trig_legit_triggerbot_hitchance,
		0
	};

	Cvar_s* Legit_TriggerbotFilter_List[] =
	{
		&trig_legit_spot_head,
		&trig_legit_spot_neck,
		&trig_legit_spot_highspine,
		&trig_legit_spot_lowspine,
		&trig_legit_spot_pelvis,
		&trig_legit_spot_thigh,
		&trig_legit_spot_upperarm,
		&trig_legit_spot_forearm,
		&trig_legit_spot_calf,
		0,
	};

	Cvar_s* Legit_Pistols_List[]
	{
		&aim_legit_pistol_fov,
		&aim_legit_pistol_recoil,
		&aim_legit_pistol_hitcapsule,
		&aim_legit_pistol_autowall,
		0,
	};

	Cvar_s* Legit_Rifles_List[]
	{
		&aim_legit_rifle_fov,
		&aim_legit_rifle_recoil,
		&aim_legit_rifle_hitcapsule,
		&aim_legit_rifle_autowall,
		0,
	};

	Cvar_s* Legit_Snipers_List[]
	{
		&aim_legit_sniper_fov,
		&aim_legit_sniper_recoil,
		&aim_legit_sniper_hitcapsule,
		&aim_legit_sniper_autowall,
		0,
	};

	Cvar_s* Rage_Aimbot_List[] = 
	{
		&aim_rage_aimbot,
		&aim_rage_autoshoot,
		&aim_rage_noviewchange,
		&aim_rage_bodyaim,
		0
	};

	Cvar_s* Rage_Accuracy_List[] =
	{
		&accuracy_rage_norecoil,
		&accuracy_rage_nospread,
		&accuracy_rage_autowall,
		&accuracy_rage_positionadjustment,
		0
	};

	Cvar_s* Rage_Target_List[] =
	{
		&aim_rage_mode,
		&aim_rage_lock,
		&aim_rage_friendlyfire,
		0
	};

	Cvar_s* Rage_AimPoint_List[] =
	{
		&aim_rage_spot_head,
		&aim_rage_spot_neck,
		&aim_rage_spot_highspine,
		&aim_rage_spot_lowspine,
		&aim_rage_spot_pelvis,
		&aim_rage_spot_thigh,
		&aim_rage_spot_upperarm,
		&aim_rage_spot_forearm,
		&aim_rage_spot_calf,
		0,
	};

	Cvar_Group Legit_Aimbot_Group[] =
	{
		{ Legit_Aimbot_List, LegitAimbot },
		{ Legit_Triggerbot_List, Triggerbot },
		{ Legit_TriggerbotFilter_List, TriggerbotFilter },
		{ Legit_Pistols_List, Pistols },
		{ Legit_Rifles_List, Rifles },
		{ Legit_Snipers_List, Snipers },
		0,
	};

	Cvar_Group Rage_Aimbot_Group[] =
	{
		{ Rage_Aimbot_List, RageAimbot },
		{ Rage_Accuracy_List, Accuracy },
		{ Rage_Target_List, TargetSelection },
		{ Rage_AimPoint_List, TargetPoints },
		0,
	};

	Cvar_Group Misc_Group[] =
	{
		0,
	};

	Cvar_Group Visuals_Group[] =
	{
		0,
	};

	Cvar_Group Colors_Group[] =
	{
		0,
	};
		
	Cvar_Parent Legit_Tab[] =
	{
		{ Legit_Aimbot_Group, g_LegitTabName },
		0
	};

	Cvar_Parent Rage_Tab[] =
	{
		{ Rage_Aimbot_Group, g_RageTabName },
		0
	};
	
	Cvar_Parent Misc_Tab[] =
	{
		{ Misc_Group, g_MiscTabName },
		0
	};

	Cvar_Parent Visuals_Tab[] =
	{
		{ Visuals_Group, g_VisualsTabName },
		0
	};

	Cvar_Parent Colors_Tab[] =
	{
		{ Colors_Group, g_ColorsTabName },
		0
	};
	
	
};
//==================================================================================================================================