#define GUI_HEIGHT 800
#define GUI_WIDTH 800
//==================================================================================================================================
#define GROUP_WIDTH 200
//==================================================================================================================================
#define MAX_TABS 5
//==================================================================================================================================
#define TAB_HEIGHT 50
#define TAB_WIDTH ( GUI_WIDTH / MAX_TABS )
//==================================================================================================================================
#define TOPBAR_HEIGHT MENU_FONT_SIZE * 2
//==================================================================================================================================
extern int itoa(int value, char *sp, int radix);
//==================================================================================================================================
typedef struct Tab_s
{
	bool m_Active;

	Cvar_Parent* Parent;
}Tab_t;
//==================================================================================================================================
class CGUI
{
public:
	
	bool CheckForClick ( int X, int Y, int W, int H );
	bool CheckForClick2 ( int X, int Y, int W, int H );

	bool GetActive() { return m_Active; }

	void Toggle()
	{
		m_Active = !m_Active;

		if ( m_Active == false )
		{
			m_Picked = false;

			for ( size_t Iterator = 0; Iterator < MAX_TABS; Iterator++ )
			{
				m_Tabs[ Iterator ].m_Active = false;
			}
		}
	}

	void ClickToggle() { m_Clicked = !m_Clicked; }
	
	void DrawGUI();
	void MoveGUI();
	void DrawMouse();

	void SetXY ( int X, int Y );
	void SetWidthAndHeight ( int Width, int Height );

	void SetMouseXY ( int X, int Y );
				
public:

	bool m_Active;
	bool m_Clicked;
	bool m_IsWaiting;
	bool m_Picked;

	int m_MouseX, m_MouseY;

	int m_X, m_MouseDeltaX, m_LastMouseX;
	int m_Y, m_MouseDeltaY, m_LastMouseY;

	uintptr_t m_Counter;

	int m_Width;
	int m_Height;

	int m_ScreenSizeX;
	int m_ScreenSizeY;

	int m_CurrentTab;
	int m_LastTab;

	Tab_t m_Tabs[MAX_TABS];

	CUserCmd Dummy;

	QAngle LastAngle;
};
//==================================================================================================================================
extern CGUI g_GUI;
//==================================================================================================================================