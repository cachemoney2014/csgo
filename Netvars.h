//=============================================================================================================
extern void HookProxy ( uintptr_t Function, uintptr_t Address, uintptr_t Base, uintptr_t DataBase, uintptr_t DataSize );
//=============================================================================================================
extern void OriginProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut);
extern void PunchAngleProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut);
extern void SimulationTimeProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut);
extern void EyeAnglesPitchProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut);
extern void EyeAnglesYawProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut);
//=============================================================================================================