class QAngle;
class Vector;

class Vector
{
	public:

		Vector::Vector ( void ) : x ( g_0 ), y ( g_0 ), z ( g_0 )
		{

		}

		Vector::Vector ( float X, float Y, float Z )
		: x ( X ), y ( Y ), z ( Z )
		{

		}

		Vector::Vector ( float* p )
		{
			*this = p;
		};

		Vector::Vector ( const Vector& in )
		{
			x = in.x;
			y = in.y;
			z = in.z;
		};
		
		inline Vector& operator= ( const Vector& in );
		inline Vector& operator= ( float* p );
		inline Vector& operator= ( float f );
		
		inline float& operator[] ( int i ) const;
		
		inline bool operator! ( void ) const;
		
		inline bool operator== ( const Vector& other ) const;

		inline bool operator!= ( const Vector& other ) const;

		inline Vector& operator+= ( const Vector& other );
		inline Vector& operator+= ( float* p );
		inline Vector& operator+= ( float f );

		inline Vector& operator-= ( const Vector& other );
		inline Vector& operator-= ( float* p );
		inline Vector& operator-= ( float f );
		
		inline Vector& operator*= ( const Vector& other );
		inline Vector& operator*= ( float *p );
		inline Vector& operator*= ( float f );
		
		inline Vector& operator/= ( const Vector& other );
		inline Vector& operator/= ( float* p );
		inline Vector& operator/= ( float f );
		
		inline Vector operator+ ( const Vector& other ) const;
		inline Vector operator+ ( float* p ) const;
		inline Vector operator+ ( float f ) const;
		
		inline Vector operator- ( const Vector& other ) const;
		inline Vector operator- ( float* p ) const;
		inline Vector operator- ( float f ) const;
		inline Vector operator- ( void ) const;
		
		inline Vector operator* ( const Vector& other ) const;
		inline Vector operator* ( float* p ) const;
		inline Vector operator* ( float f ) const;
		
		inline Vector operator/ ( const Vector& other ) const;
		inline Vector operator/ ( float* p ) const;
		inline Vector operator/ ( float f ) const;

		operator float *()								{ return &x; }
		operator const float *() const					{ return &x; }
		
		inline bool Vector::IsZero ( void ) const
		{
			return x == g_0 && y == g_0 && z == g_0;
		}

		inline bool Vector::IsZero2D ( void )
		{
			return x == g_0 && y == g_0;
		}

		inline Vector& Clear ( void )
		{
			x = y = z = g_0;
						
			return *this;
		}

		inline Vector& Init()
		{
			x = y = z = g_0;

			return *this;
		}
		
		inline Vector& Init ( float X, float Y, float Z )
		{
			x = X;
			y = Y;
			z = Z;

			return *this;
		}
				
		inline Vector& Init ( float* p )
		{
			*this = p;

			return *this;
		}

		inline Vector& Negate ( void )
		{
			x = -x;
			y = -y;
			z = -z;

			return *this;
		}

		inline float Dot ( const Vector& other )
		{
			return x*other.x + y*other.y + z*other.z;
		}

		inline float Dot2D ( const Vector& other )
		{
			return x*other.x + y*other.y;
		}

		inline float Length ( void )
		{
			float Length = g_0;

			Length = _sqrt ( LengthSqr() );
	
			return Length;
		}

		inline float Length2D ( void )
		{
			float Length = g_0;

			Length = _sqrt ( LengthSqr2D() );
	
			return Length;
		}

		inline float LengthSqr ( void )
		{
			return x*x + y*y + z*z;
		}

		inline float LengthSqr2D ( void )
		{
			return x*x + y*y;
		}

		inline float Distance ( Vector& ToVector )
		{
			return ( ToVector - *this ).Length();
		}

		inline float Distance2D ( Vector& ToVector )
		{
			return ( ToVector - *this ).Length2D();
		}

		inline float Normalize ( void )
		{
			float VectorLength = Length();
			float RecipLength;

			if ( VectorLength != g_0 )
			{
				RecipLength = Get1() / VectorLength;

				x *= RecipLength;
				y *= RecipLength;
				z *= RecipLength;
			}

			return VectorLength;
		}

		inline float Normalize2D ( void )
		{
			float Length = LengthSqr2D();
			float RecipLength;

			if ( Length != g_0 )
			{
				RecipLength = Get1() / Length;

				x *= RecipLength;
				y *= RecipLength;
			}

			return Length;
		}

		inline float AngleBetween ( Vector& other )
		{
			other.Normalize();
			Normalize();

			return _acos ( Dot ( other ) ) * g_RadiansToDegrees;
		}

		inline Vector &Transform ( Vector& in1, matrix3x4_t& in2 )
		{
			x = in1.Dot ( in2[0] ) + in2[0][3];
			y = in1.Dot ( in2[1] ) + in2[1][3];
			z = in1.Dot ( in2[2] ) + in2[2][3];

			return *this;
		}

		Vector &CrossProduct ( const Vector& a, const Vector& b )
		{
			x = ( a.y * b.z ) - ( a.z * b.y );
			y = ( a.z * b.x ) - ( a.x * b.z );
			z = ( a.x * b.y ) - ( a.y * b.x );

			return *this;
		}
		QAngle ToEulerAngles();
		QAngle ToEulerAngles ( Vector* PseudoUp );
		void AngleMatrix ( QAngle& Rotation, float ( *matrix )[3] );
		void VectorRotate ( Vector& In, QAngle& Rotation );

		void VectorVectors ( Vector& Right, Vector& Up )
		{
			Vector tmp;

			if ( x == g_0 && y == g_0)
			{
				Right[0] = g_1;	
				Right[1] = g_0; 
				Right[2] = g_0;
				Up[0] = -z; 
				Up[1] = g_0; 
				Up[2] = g_0;
				return;
			}

			tmp[0] = g_0; tmp[1] = g_0; tmp[2] = g_1;
			CrossProduct( tmp, Right );
			Right.Normalize();
			Right.CrossProduct( *this, Up );
			Up.Normalize();
		}

		inline bool IsValid()
		{
			return IsFinite ( x ) && IsFinite ( y ) && IsFinite ( z );
		}

	public:

		float x, y, z;
};

inline Vector& Vector::operator= ( const Vector& in )
{
	x = in.x;
	y = in.y;
	z = in.z;

	return *this;
}

inline Vector& Vector::operator= ( float* p )
{
	if ( p )
	{
		x = p[0]; y = p[1]; z = p[2];
	}
	else
	{
		x = y = z = g_0;
	}

	return *this;
}

inline Vector& Vector::operator= ( float f )
{
	x = y = z = f;

	return *this;
}

inline float &Vector::operator[] ( int i ) const
{
	if ( i >= 0 && i < 3 )
	{
		return ( ( float* )this )[i];
	}

	return ( ( float* )this )[0];
}

inline bool Vector::operator! ( void ) const
{
	return IsZero();
}

inline bool Vector::operator== ( const Vector& other ) const
{
	return x == other.x && y == other.y && z == other.z;
}

inline bool Vector::operator!= ( const Vector& other ) const
{
	return x != other.x || y != other.y || z != other.z;
}

inline Vector& Vector::operator+= ( const Vector& other )
{
	x += other.x;
	y += other.y;
	z += other.z;

	return *this;
}

inline Vector& Vector::operator+= ( float* p )
{
	x += p[0];
	y += p[1];
	z += p[2];

	return *this;
}

inline Vector& Vector::operator+= ( float f )
{
	x += f;
	y += f;
	z += f;

	return *this;
}

inline Vector& Vector::operator-= ( const Vector& other )
{
	x -= other.x;
	y -= other.y;
	z -= other.z;

	return *this;
}

inline Vector& Vector::operator-= ( float* p )
{
	x -= p[0];
	y -= p[1];
	z -= p[2];

	return *this;
}
inline Vector& Vector::operator-= ( float f )
{
	x -= f;
	y -= f;
	z -= f;

	return *this;
}

inline Vector& Vector::operator*= ( const Vector& other )
{
	x *= other.x;
	y *= other.y;
	z *= other.z;

	return *this;
}

inline Vector& Vector::operator*= ( float *p )
{
	x *= p[0];
	y *= p[1];
	z *= p[2];

	return *this;
}

inline Vector& Vector::operator*= ( float f )
{
	x *= f;
	y *= f;
	z *= f;

	return *this;
}

inline Vector& Vector::operator/= ( const Vector& other )
{
	if ( other.x != g_0 && other.y != g_0 && other.z != g_0 )
	{
		x /= other.x;
		y /= other.y;
		z /= other.z;
	}

	return *this;
}

inline Vector& Vector::operator/= ( float* p )
{
	if ( p[0] != g_0 && p[1] != g_0 && p[2] != g_0 )
	{
		x /= p[0];
		y /= p[1];
		z /= p[2];
	}

	return *this;
}

inline Vector& Vector::operator/= ( float f )
{
	if ( f != g_0 )
	{
		x /= f;
		y /= f;
		z /= f;
	}

	return *this;
}

inline Vector Vector::operator+ ( const Vector& other ) const
{
	return Vector ( x + other.x, y + other.y, z + other.z );
}

inline Vector Vector::operator+ ( float* p ) const
{
	return Vector ( x + p[0], y + p[1], z + p[2] );
}

inline Vector Vector::operator+ ( float f ) const
{
	return Vector ( x + f, y + f, z + f );
}

inline Vector  Vector::operator- ( const Vector& other ) const
{
	return Vector ( x - other.x, y - other.y, z - other.z );
}

inline Vector  Vector::operator- ( float* p ) const
{
	return Vector ( x - p[0], y - p[1], z - p[2] );
}

inline Vector  Vector::operator- ( float f ) const
{
	return Vector ( x - f, y - f, z - f );
}

inline Vector Vector::operator- ( void ) const
{
	return Vector ( -x, -y, -z );
}

inline Vector Vector::operator* ( const Vector& other ) const
{
	return Vector ( x * other.x, y * other.y, z * other.z );
}

inline Vector Vector::operator* ( float* p ) const
{
	return Vector ( x * p[0], y * p[1], z * p[2] );
}

inline Vector Vector::operator* ( float f ) const
{
	return Vector ( x * f, y * f, z * f );
}

inline Vector Vector::operator/ ( const Vector& other ) const
{
	if ( other.x != g_0 && other.y != g_0 && other.z != g_0 )
	{
		return Vector ( x / other.x, y / other.y, z / other.z );
	}

	return *this;
}

inline Vector Vector::operator/ ( float* p ) const
{
	if ( p[0] != g_0 && p[1] != g_0 && p[2] != g_0 )
	{
		return Vector ( x / p[0], y / p[1], z / p[2] );
	}

	return *this;
}

inline Vector Vector::operator/ ( float f ) const
{
	if ( f != g_0 )
	{
		return Vector ( x / f, y / f, z / f );
	}

	return *this;
}

inline Vector operator*(float fl, const Vector& v)	{ return v * fl; };