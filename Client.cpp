//==================================================================================================================================
#include "Required.h"
#include "Cvars.h"
#include "Memory.h"
#include "Aimbot.h"
#include "Client.h"
#include "GUI.h"
#include "Hook.h"
#include "Init.h"
//==================================================================================================================================
CSGO_LocalPlayer_t CSGO_LocalPlayer;
CSGO_Player_t CSGO_Player;
//==================================================================================================================================
typedef void ( __stdcall* OverrideMouseInput_t )( float *x, float *y );
typedef bool ( __stdcall* ClientMode_CreateMove_t )( float flInputSampleTime, CUserCmd* pCmd );
typedef int ( __stdcall* ClientMode_HudElementKeyInput_t )( int down, ButtonCode_t keynum, const char *pszCurrentBinding );
typedef int ( __stdcall* IN_KeyEvent_t )( int eventcode, ButtonCode_t keynum, const char *pszCurrentBinding );
typedef void ( __stdcall* CreateMove_t )( int sequence_number, float input_sample_frametime, bool active );
//==================================================================================================================================
typedef void ( __stdcall* SetViewAngles_t )( Vector&va );
//==================================================================================================================================
/*
void HackOverrideMouseInput ( float *x, float *y )
{
	g_GUI.SetMouseXY ( ( int )*x, ( int )*y );
}
//==================================================================================================================================
void __stdcall ClientMode_OverrideMouseInput_Hook ( float *x, float *y )
{
	CallVirtual<OverrideMouseInput_t>( ClientModeHook.GetOriginalDataPointer(), CSGO_CLIENTDLL_CLIENTMODE_RDATA_OverrideMouseInput )( x, y );
	
	HackOverrideMouseInput ( x, y );

	return;
}
*/
//==================================================================================================================================
void HackCreateMove ( CUserCmd* pCmd )
{
	int OldCommandNumber, OldTickCount;

	if ( g_GUI.GetActive() )
	{		
		OldCommandNumber = pCmd->command_number;
		OldTickCount = pCmd->tick_count;

		memcpy ( pCmd, &g_GUI.Dummy, sizeof ( CUserCmd ) );
				
		pCmd->tick_count = OldTickCount;
		pCmd->command_number = OldCommandNumber;
	}
	else
	{
		memcpy ( &g_GUI.Dummy, pCmd, sizeof ( CUserCmd ) );
	}
}
/*
//==================================================================================================================================
bool __stdcall ClientMode_CreateMove_Hook ( float flInputSampleTime, CUserCmd* pCmd )
{
	bool Return = CallVirtual<ClientMode_CreateMove_t>( ClientModeHook.GetOriginalDataPointer(), CSGO_CLIENTDLL_CLIENTMODE_RDATA_ClientModeCreateMove )( flInputSampleTime, pCmd );

	HackCreateMove ( pCmd );

	return Return;
}
//==================================================================================================================================
void HackKeyInput ( int down, ButtonCode_t keynum )
{
	if ( down && keynum == KEY_INSERT )
	{
		g_GUI.Toggle();
	}
	if ( keynum == MOUSE_LEFT )
	{
		g_GUI.ClickToggle();
	}
}
//==================================================================================================================================
int __stdcall ClientMode_HudElementKeyInput_Hook ( int down, ButtonCode_t keynum, const char *pszCurrentBinding )
{
	int Return = CallVirtual<ClientMode_HudElementKeyInput_t>( ClientModeHook.GetOriginalDataPointer(), CSGO_CLIENTDLL_CLIENTMODE_RDATA_HudElementKeyInput )( down, keynum, pszCurrentBinding );

	HackKeyInput ( down, keynum );

	return Return;
}
*/
//==================================================================================================================================
int __stdcall IN_KeyEvent_Hook ( int eventcode, ButtonCode_t keynum, const char *pszCurrentBinding )
{
	int Return = CallVirtual<IN_KeyEvent_t>( VClient018.GetOriginalDataPointer(), CSGO_CLIENTDLL_VClient018_IN_KeyEvent )( eventcode, keynum, pszCurrentBinding );

	if ( eventcode && keynum == KEY_INSERT )
	{
		g_GUI.Toggle();
	}
	
	return Return;
}
//==================================================================================================================================
void __stdcall CreateMove_Hook ( int sequence_number, float input_sample_frametime, bool active )
{	
	CallVirtual<CreateMove_t>( VClient018.GetOriginalDataPointer(), CSGO_CLIENTDLL_VClient018_CreateMove )( sequence_number, input_sample_frametime, active );
}
//==================================================================================================================================
void SetViewAngles_Hook ( Vector& va )
{
	CallVirtual<SetViewAngles_t>( IVEngineClient014.GetOriginalDataPointer(), 19 )( va );
}
//==================================================================================================================================