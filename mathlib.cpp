//==================================================================================================================================
float _acos ( float Cosine )
{
	float ArcCosine;

	__asm
	{
		fld Cosine
		fld Cosine
		fmul
		fld1
		fsubr
		fsqrt
		fxch 
		fpatan
		fstp ArcCosine
	}

	return ArcCosine;
}
//==================================================================================================================================
float _atan ( float Tangent )
{
	float ArcTangent;

	__asm
	{
		fld	Tangent
		fld1
		fpatan
		fstp ArcTangent;
	}

	return ArcTangent;
}
//==================================================================================================================================
float _atan2 ( float Sine, float Cosine )
{
	float ArcTangent;

	__asm
	{
		fld Sine
		fld Cosine
		fpatan
		fstp ArcTangent
	}

	return ArcTangent;
}
//==================================================================================================================================
float _sqrt ( float AngleInRadians )
{
	float SquareRoot;

	__asm
	{
		fld AngleInRadians
		fsqrt
		fstp SquareRoot
	}

	return SquareRoot;
}
//==================================================================================================================================
float _cos ( float AngleInRadians )
{
	float Cosine;

	__asm
	{
		fld AngleInRadians
		fcos
		fstp Cosine
	}

	return Cosine;
}
//==================================================================================================================================
float _sin ( float AngleInRadians )
{
	float Sine;

	__asm
	{
		fld AngleInRadians
		fsin
		fstp Sine
	}

	return Sine;
}
//==================================================================================================================================
float Get1()
{
	float One;

	__asm
	{
		FLD1
		FSTP One;
	}

	return One;
}
//==================================================================================================================================