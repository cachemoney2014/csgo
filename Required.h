#if !defined (_W64)
#if !defined (__midl) && (defined (_X86_) || defined (_M_IX86))
#define _W64 __w64
#else  /* !defined (__midl) && (defined (_X86_) || defined (_M_IX86)) */
#define _W64
#endif  /* !defined (__midl) && (defined (_X86_) || defined (_M_IX86)) */
#endif  /* !defined (_W64) */

#ifndef _UINTPTR_T_DEFINED
#ifdef _WIN64
typedef unsigned __int64    uintptr_t;
typedef long long LONG_PTR;
typedef long long LONGLONG_PTR;
typedef unsigned long long ULONG_PTR;
typedef unsigned long long ULONGLONG_PTR;
#else  /* _WIN64 */
typedef _W64 unsigned int   uintptr_t;
typedef long long LONG_PTR;
typedef long long LONGLONG_PTR;
typedef unsigned long ULONG_PTR;
typedef unsigned long ULONGLONG_PTR;
#endif  /* _WIN64 */
#define _UINTPTR_T_DEFINED
#endif  /* _UINTPTR_T_DEFINED */

#define WINAPI __stdcall

#define TRUE 1
#define FALSE 0

typedef int BOOL;

template< typename Function > Function CallVirtual ( void* ClassBase, uintptr_t Index )
{
	uintptr_t** VTablePointer = ( uintptr_t** )ClassBase;
	uintptr_t* VTableFunctionBase = *VTablePointer;
	uintptr_t Address = VTableFunctionBase[ Index ];
	
	return ( Function )( Address );
}

#include "Offsets.h"

#include "PE.h"
#include "constants.h"
#include "mathlib.h"
#include "QAngle.h"
#include "Vector.h"
#include "SDK.h"

#define CLIENT_HASH 0xf55ece87
#define ENGINE_HASH 0xae60bc92
#define VGUI2_HASH 0x14019f3d
#define VGUIMATSYSTEMSURFACE_HASH 0x20ab08f5
#define INPUTSYSTEM_HASH 0x9755800a

#define MSVCRT_HASH 0xefa366db
#define NTDLL_HASH 0x85DCD932
#define KERNEL32_HASH 0x6ff346e
#define USER32_HASH 0x87a22bed

#define CREATEINTERFACE_HASH 0x583c88c2

#define RTLALLOCATEHEAP_HASH 0x539BC3D1
#define RTLFREEHEAP_HASH 0xCD09A2BB
#define RAND_HASH 0xef6c6e69
#define SRAND_HASH 0x35ea3fa8
#define GETASYNCKEYSTATE_HASH 0x5c8846a6
#define GETKEYBOARDSTATE_HASH 0xc8a211c0
#define GETKEYNAMETEXT_HASH 0x3ab14ce6
#define MAPVIRTUALKEY_HASH 0x51dc769
#define MBSTOWCS_HASH 0xc0b03c57

extern "C" void *_ReturnAddress ( void );
#pragma intrinsic ( _ReturnAddress )