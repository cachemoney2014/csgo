#include "Required.h"

void Vector::AngleMatrix ( QAngle& Rotation, float ( *matrix )[3] )
{
	float sp, sy, sr, cp, cy, cr, radx, rady, radz;

	radx = Rotation.x * ( g_2PI / g_360 );
	rady = Rotation.y * ( g_2PI / g_360 );
	radz = Rotation.z * ( g_2PI / g_360 );

	sp = _sin ( radx ); 
	sy = _sin ( rady ); 
	sr = _sin ( radz ); 

	cp = _cos ( radx );
	cy = _cos ( rady );
	cr = _cos ( radz );
		
	matrix[0][0] = cp * cy;
	matrix[0][1] = cp * sy;
	matrix[0][2] = -sp;
	
	matrix[1][0] = sr * sp * cy + cr * -sy;
	matrix[1][1] = sr * sp * sy + cr * cy;
	matrix[1][2] = sr * cp;
		
	matrix[2][0] = cr * sp * cy + -sr * -sy;
	matrix[2][1] = cr * sp * sy + -sr * cy;
	matrix[2][2] = cr * cp;
}

void Vector::VectorRotate ( Vector& In, QAngle& Rotation )
{
	float matRotate[3][3];

	AngleMatrix ( Rotation, matRotate );

	x = In.Dot( matRotate[0] );
	y = In.Dot( matRotate[1] );
	z = In.Dot( matRotate[2] );
}

QAngle Vector::ToEulerAngles()
{
	float Pitch, Yaw, Length;

	Length = Length2D();

	if ( Length > g_0 )
	{
		Pitch = ( _atan2 ( -z, Length ) * g_RadiansToDegrees );

		if ( Pitch < g_0 )
		{
			Pitch += g_360;
		}

		Yaw = ( _atan2 ( y, x ) * g_RadiansToDegrees );

		if ( Yaw < g_0 )
		{
			Yaw += g_360;
		}
	}
	else
	{
		Pitch = ( z > g_0 ) ? g_270 : g_90;
		Yaw = g_0;
	}
			
	return QAngle ( Pitch, Yaw, g_0 );
}

QAngle Vector::ToEulerAngles ( Vector* PseudoUp )
{
	Vector Left;

	float	Length, Yaw, Pitch, Roll;

	Left.CrossProduct ( *PseudoUp, *this );
	
	Left.Normalize();
		
	Length = Length2D();

	if ( PseudoUp )
	{        
		if ( Length > g_001 )
		{                   							
			Pitch = ( _atan2 ( -z, Length ) * g_RadiansToDegrees );

			if ( Pitch < g_0 )
			{
				Pitch += g_360;
			}
        
			Yaw = ( _atan2 ( y, x ) * g_RadiansToDegrees );

			if ( Yaw < g_0 )
			{
				Yaw += g_360;
			}

			float up_z = ( Left[1] * x ) - ( Left[0] * y );
                                                     
			Roll = ( _atan2 ( Left[2], up_z ) * g_RadiansToDegrees );

			if ( Roll < g_0 )
			{
				Roll += g_360;
			}
		}
		else
		{
			Yaw = ( _atan2 ( y, x ) * g_RadiansToDegrees );

			if ( Yaw < g_0 )
			{
				Yaw += g_360;
			}
		
			Pitch = ( _atan2 ( -z, Length ) * g_RadiansToDegrees );
		
			if ( Pitch < g_0 )
			{
				Pitch += g_360;
			}

			Roll = g_0;
		}
	}
	

	return QAngle ( Pitch, Yaw, Roll );
}