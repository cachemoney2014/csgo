//==================================================================================================================================
#include "Required.h"
#include "Memory.h"
#include "Client.h"
#include "Cvars.h"
#include "Drawing.h"
#include "GUI.h"
#include "Hook.h"
#include "Init.h"
#include "Netvars.h"
#include "Rendering.h"
#include "Strings.h"
//==================================================================================================================================
CVMTHook ClientModeHook;
CExternalVMTHook VGUIMatSurfaceVGUIPanel009;
CExternalVMTHook VClient018;
CExternalVMTHook IVEngineClient014;
//==================================================================================================================================
CEncryptedAddressPointer g_EncryptedPanel;
CEncryptedAddressPointer g_EncryptedSurface;
CEncryptedAddressPointer g_EncryptedEngineClient;
CEncryptedAddressPointer g_EncryptedGetCursorPos;
CEncryptedAddressPointer g_EncryptedInputSystem;
CEncryptedAddressPointer g_EncryptedGetAsyncKeyState;
CEncryptedAddressPointer g_GetKeyboardState;
CEncryptedAddressPointer g_GetKeyNameText;
CEncryptedAddressPointer g_EncryptedMBSTOWCS;
CEncryptedAddressPointer g_MapVirtualKey;
//==================================================================================================================================
#include "wipe.h"
#include "wipe_start.h"
//==================================================================================================================================
uintptr_t g_ReturnAddress;
uintptr_t g_SectionBase;
uintptr_t g_VirtualFree;
//==================================================================================================================================
void WipeSection ( void );
//==================================================================================================================================
__declspec ( naked ) void Setup ( void )
{
	__asm
	{
		MOV EAX, DWORD PTR SS:[ESP]
		NOP
		NOP
		MOV g_ReturnAddress, EAX
		ADD ESP, 4
		CALL WipeSection
		MOV EAX, g_VirtualFree
		PUSH MEM_DECOMMIT
		PUSH 0x1000
		PUSH g_SectionBase
		PUSH g_ReturnAddress
		MOV g_ReturnAddress, 0
		MOV g_SectionBase, 0
		MOV g_VirtualFree, 0
		JMP EAX
	}
}
//==================================================================================================================================
void WipeSection ( void )
{

}
//==================================================================================================================================
RtlAllocateHeap_t RtlAllocateHeap;
RtlFreeHeap_t RtlFreeHeap;
rand_t rand;
srand_t srand;
//==================================================================================================================================
HANDLE g_hProcessHeap;
//==================================================================================================================================
HANDLE GetHeap ( void )
{
	HANDLE hHeap;

	__asm
	{
		MOV EAX, DWORD PTR FS:[ 0x18 ]
		MOV EAX, DWORD PTR DS:[ EAX+0x30 ]
		MOV EAX, DWORD PTR DS:[ EAX+0x18 ]
		MOV hHeap, EAX
	}

	return hHeap;
}
//==================================================================================================================================
PVOID __stdcall AllocateFromHeap ( DWORD dwSize )
{	
	PVOID pvMem = NULL;
		
	pvMem = RtlAllocateHeap ( GetHeap(), 0, dwSize );
				
	return pvMem;
}
//==================================================================================================================================
void __stdcall DeallocateOffHeap ( PVOID pvMemory )
{
	RtlFreeHeap ( GetHeap(), 0, pvMemory );
}
//==================================================================================================================================
#define WIDTH 32
#define POLYNOMIAL ( 0x488781ED )
#define TOPBIT ( 1 << ( WIDTH - 1 ) )
//==================================================================================================================================
DWORD __stdcall CRCBlock ( DWORD Remainder, PVOID Block, int BlockSize ) // ripped from Miranda, hey VAC can do it why can't we?
{
	int Bit, Byte;
	PBYTE pBlock;

	Bit = 0;
	Byte = 0;

	pBlock = ( PBYTE )Block;
	
	for ( Byte = 0; Byte < BlockSize; Byte++ ) 
	{
		Remainder ^= ( pBlock [ Byte ] << ( WIDTH - 8 ) );

		for ( Bit = 8; Bit > 0; --Bit) 
		{
			if ( Remainder & TOPBIT )
			{
				Remainder = ( Remainder << 1 ) ^ POLYNOMIAL;
			}
			else
			{
				Remainder = ( Remainder << 1 );
			}
		}
	}

	return Remainder;
}
//==================================================================================================================================
void __stdcall ustrcpytolower ( PUNICODE_STRING dst, PUNICODE_STRING src )
{
	size_t i;
	WCHAR c;
	
	dst->Length = src->Length;
	
	for ( i = 0; i < ( src->Length / sizeof ( WCHAR ) ); i++ )
	{
		c = src->Buffer[i];
		
		if ( c >= 0x41 && c <= 0x5A )
			c |= 0x20;
			
		dst->Buffer[i] = c;
	}
	
	dst->Buffer[i] = 0x00;
}
//=============================================================================================================
HMODULE __stdcall GetModuleHandle ( DWORD dwHash )
{
	PPROCESS_ENVIRONMENT_BLOCK peb;
	PLDR_DATA_TABLE_ENTRY module;
	PLIST_ENTRY entry;
	PLIST_ENTRY InLoadOrderModuleList;
	UNICODE_STRING basedllname;
	WCHAR buffer [ MAX_PATH ];
	
	basedllname.MaximumLength = MAX_PATH;
	basedllname.Buffer = ( PWSTR )buffer;
    
	peb = GetPEB();
    
	if ( peb != NULL )
	{
		InLoadOrderModuleList = &peb->Ldr->InLoadOrderModuleList;

		for ( entry = InLoadOrderModuleList->Flink; entry != NULL, entry != InLoadOrderModuleList; entry = entry->Flink )
		{ 
			module = CONTAINING_RECORD ( entry, LDR_DATA_TABLE_ENTRY, InLoadOrderModuleList );
            
			ustrcpytolower ( &basedllname, &module->BaseDllName );
            
			if ( dwHash == CRCBlock ( 0x0, basedllname.Buffer, basedllname.Length ) )
			{
				return ( HMODULE )module->BaseAddress;
			}
		}
	} 
    
	return ( HMODULE )NULL;
}
//==================================================================================================================================
DWORD __stdcall GetProcAddress ( HMODULE hModule, DWORD Hash ) 
{
	int iLength;

	WORD i;
	DWORD dwExport, dwModuleBase, dwHash, dwName;

	PIMAGE_DOS_HEADER doshdr;
	PIMAGE_NT_HEADERS nthdr;
	PIMAGE_OPTIONAL_HEADER32 opthdr;
	PIMAGE_DATA_DIRECTORY datadir;
	PIMAGE_EXPORT_DIRECTORY exportdir;
    
	PWORD  ordtable;
	PDWORD addrtable;
	PDWORD nametable;
    
	PCHAR pszProcname;

	dwModuleBase = ( DWORD )hModule;
	doshdr = ( PIMAGE_DOS_HEADER )dwModuleBase;
    
	if ( doshdr->e_magic != IMAGE_DOS_SIGNATURE )
	{
		return 0;
	}

	nthdr = ( PIMAGE_NT_HEADERS ) ( dwModuleBase + doshdr->e_lfanew );
    
	if ( nthdr->Signature != IMAGE_NT_SIGNATURE )
	{
		return 0;
	}
        
	opthdr = &nthdr->OptionalHeader;
	datadir = &opthdr->DataDirectory [ IMAGE_DIRECTORY_ENTRY_EXPORT ];
	exportdir = ( PIMAGE_EXPORT_DIRECTORY )( dwModuleBase + datadir->VirtualAddress );

	ordtable = ( PWORD )( dwModuleBase + ( DWORD )exportdir->AddressOfNameOrdinals );
	addrtable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir->AddressOfFunctions ); 
	nametable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir->AddressOfNames ); 

	i = 0;
	iLength = 0;
	dwHash = 0;
	dwName = 0;
	
	for ( ; i < exportdir->NumberOfNames; i++ )
	{
		dwName = nametable [ i ];

		pszProcname = ( PCHAR )( dwModuleBase + dwName );

		iLength = strlen ( pszProcname );

		dwHash = CRCBlock ( 0x0, pszProcname, iLength );

		if ( Hash == dwHash )
		{
			dwExport = dwModuleBase + addrtable [ ordtable [ i ] ];
		}
		else
		{
			continue;
		}
	}
	
	return dwExport;
}
//==================================================================================================================================
void DLLStartup ( uintptr_t ModuleBase, LPVOID lpReserved )
{
	uintptr_t MSVCRTDLL = ( uintptr_t )GetModuleHandle ( MSVCRT_HASH );
	uintptr_t EngineDLL = ( uintptr_t )GetModuleHandle ( ENGINE_HASH );
	uintptr_t ClientDLL = ( uintptr_t )GetModuleHandle ( CLIENT_HASH );
	uintptr_t VGUI2DLL = ( uintptr_t )GetModuleHandle ( VGUI2_HASH );
	uintptr_t USER32DLL = ( uintptr_t )GetModuleHandle ( USER32_HASH );
	uintptr_t VGUIMATSYSTEMSURFACE = ( uintptr_t )GetModuleHandle ( VGUIMATSYSTEMSURFACE_HASH );
	uintptr_t InputSystemDLL = ( uintptr_t )GetModuleHandle ( INPUTSYSTEM_HASH );
	uintptr_t KERNEL32DLL = ( uintptr_t )GetModuleHandle ( KERNEL32_HASH );
	
	uintptr_t NTDLL = ( uintptr_t )GetModuleHandle ( NTDLL_HASH );

	RtlAllocateHeap = ( RtlAllocateHeap_t )GetProcAddress ( ( HMODULE )NTDLL, RTLALLOCATEHEAP_HASH );
	RtlFreeHeap = ( RtlFreeHeap_t )GetProcAddress ( ( HMODULE )NTDLL, RTLFREEHEAP_HASH );

	rand = ( rand_t )GetProcAddress ( ( HMODULE )MSVCRTDLL, RAND_HASH );
	srand = ( srand_t )GetProcAddress ( ( HMODULE )MSVCRTDLL, SRAND_HASH );

	srand ( ~rand() + EngineDLL + ~CSGO_ENGINEDLL_VClient018_RVADisplacement );

	VClient018.SetKey ( ~rand() + ~EngineDLL + ~CSGO_ENGINEDLL_VClient018_RVADisplacement );
		
	VClient018.Init ( EngineDLL + CSGO_ENGINEDLL_VClient018_RVADisplacement, ClientDLL + CSGO_CLIENTDLL_SECTION_TEXT_RVA, ClientDLL + CSGO_CLIENTDLL_SECTION_TEXT_RVA + CSGO_CLIENTDLL_SECTION_TEXT_SIZE );
	
	VClient018.Hook ( ( uintptr_t )IN_KeyEvent_Hook, CSGO_CLIENTDLL_VClient018_IN_KeyEvent );
	VClient018.Hook ( ( uintptr_t )CreateMove_Hook, CSGO_CLIENTDLL_VClient018_CreateMove );

	IVEngineClient014.Init ( ClientDLL + CSGO_CLIENTDLL_IVEngineClient014_RVADisplacement, EngineDLL + CSGO_ENGINEDLL_SECTION_TEXT_RVA, EngineDLL + CSGO_ENGINEDLL_SECTION_TEXT_RVA + CSGO_ENGINEDLL_SECTION_TEXT_SIZE );
	
	IVEngineClient014.Hook ( ( uintptr_t )SetViewAngles_Hook, 19 );
		
	srand ( ~rand() + VGUIMATSYSTEMSURFACE + ~CSGO_VGUIMATSURFACE_VGUI_Panel009_RVADisplacement );

	VGUIMatSurfaceVGUIPanel009.SetKey ( ~rand() + ~VGUIMATSYSTEMSURFACE + ~CSGO_VGUIMATSURFACE_VGUI_Panel009_RVADisplacement );
	
	VGUIMatSurfaceVGUIPanel009.Init ( VGUIMATSYSTEMSURFACE + CSGO_VGUIMATSURFACE_VGUI_Panel009_RVADisplacement, VGUI2DLL + CSGO_VGUI2_SECTION_TEXT_RVA, VGUI2DLL + CSGO_VGUI2_SECTION_TEXT_RVA + CSGO_VGUI2_SECTION_TEXT_SIZE );

	VGUIMatSurfaceVGUIPanel009.Hook ( ( uintptr_t )PaintTraverse_Hook, 41 );

	// todo: Write a templated class to wrap these pointers and encrypt them
	
	g_EncryptedEngineClient.SetKey ( ~rand() + ~EngineDLL );
	g_EncryptedEngineClient = EngineDLL + CSGO_ENGINEDLL_IVEngineClient_RVADisplacement;

	g_EncryptedPanel.SetKey ( ~rand() + ~VGUI2DLL );
	g_EncryptedPanel = VGUI2DLL + CSGO_VGUI2_PANEL_DATA_PTR_RVAImmediate;

	g_EncryptedSurface.SetKey ( ~rand() + ~VGUIMATSYSTEMSURFACE );
	g_EncryptedSurface = VGUIMATSYSTEMSURFACE + CSGO_VGUIMATSURFACE_RVADisplacement;

	g_EncryptedInputSystem.SetKey ( ~rand() + ~InputSystemDLL );;
	g_EncryptedInputSystem = InputSystemDLL + CSGO_INPUTSYSTEM_INPUTSYSTEM001_RVADisplacement;

	g_EncryptedGetAsyncKeyState.SetKey ( ~rand() + ~USER32DLL );
	g_EncryptedGetAsyncKeyState = ( uintptr_t )GetProcAddress ( ( HMODULE )USER32DLL, GETASYNCKEYSTATE_HASH );

	g_EncryptedMBSTOWCS.SetKey ( ~rand() + ~MSVCRTDLL );
	g_EncryptedMBSTOWCS = ( uintptr_t )GetProcAddress ( ( HMODULE )MSVCRTDLL, MBSTOWCS_HASH );

	g_GetKeyboardState.SetKey ( ~rand() + ~rand() + USER32DLL );
	g_GetKeyboardState = ( uintptr_t )GetProcAddress ( ( HMODULE )USER32DLL, GETKEYBOARDSTATE_HASH );

	g_GetKeyNameText.SetKey ( ~rand() + ~rand() + USER32DLL + ~MSVCRTDLL );
	g_GetKeyNameText = ( uintptr_t )GetProcAddress ( ( HMODULE )USER32DLL, GETKEYNAMETEXT_HASH );

	g_MapVirtualKey.SetKey ( ~rand() + ~rand() + USER32DLL + ~MSVCRTDLL + ~KERNEL32DLL );
	g_MapVirtualKey = ( uintptr_t )GetProcAddress ( ( HMODULE )USER32DLL, MAPVIRTUALKEY_HASH );
/*
	ClientModeHook.SetInformation ( ClientDLL, CSGO_CLIENTDLL_SECTION_TEXT_RVA, CSGO_CLIENTDLL_SECTION_TEXT_SIZE, // this could be handled by a call to VirtualQuery in the function
		CSGO_CLIENTDLL_SECTION_RDATA_RVA, CSGO_CLIENTDLL_SECTION_RDATA_SIZE,
		CSGO_CLIENTDLL_SECTION_DATA_RVA, CSGO_CLIENTDLL_SECTION_DATA_SIZE, CSGO_CLIENTDLL_CLIENTMODE_RDATA_VTableSize );

	srand ( ~rand() + ClientDLL + CSGO_CLIENTDLL_CLIENTMODE_DATA2_RVADisplacement );

	ClientModeHook.SetKey ( ~rand() + ~ClientDLL + ~CSGO_CLIENTDLL_CLIENTMODE_DATA2_RVADisplacement );

	ClientModeHook.Init ( ClientDLL + CSGO_CLIENTDLL_CLIENTMODE_DATA2_RVADisplacement );

	ClientModeHook.Hook ( ( uintptr_t )ClientMode_OverrideMouseInput_Hook, CSGO_CLIENTDLL_CLIENTMODE_RDATA_OverrideMouseInput );
	ClientModeHook.Hook ( ( uintptr_t )ClientMode_CreateMove_Hook, CSGO_CLIENTDLL_CLIENTMODE_RDATA_ClientModeCreateMove );
	ClientModeHook.Hook ( ( uintptr_t )ClientMode_HudElementKeyInput_Hook, CSGO_CLIENTDLL_CLIENTMODE_RDATA_HudElementKeyInput );
*/	
	memset ( &g_GUI, 0, sizeof ( g_GUI ) );

	g_EncryptedEngineClient.Get<IVEngineClient>()->GetScreenSize ( g_GUI.m_ScreenSizeX, g_GUI.m_ScreenSizeY );

	g_GUI.m_Tabs[0].Parent = cvars::Legit_Tab;
	g_GUI.m_Tabs[1].Parent = cvars::Rage_Tab;
	g_GUI.m_Tabs[2].Parent = cvars::Misc_Tab;
	g_GUI.m_Tabs[3].Parent = cvars::Visuals_Tab;
	g_GUI.m_Tabs[4].Parent = cvars::Colors_Tab;
		
	g_GUI.m_MouseX = 0;
	g_GUI.m_MouseY = 0;

	g_GUI.SetXY ( 0, 0 );
	g_GUI.SetWidthAndHeight ( GUI_HEIGHT, GUI_WIDTH );

	Cvar_Parent* pParent;
	Cvar_Group* pGroup;
	Cvar_s** ppCvar;
	Cvar_s* pCvar;

	// todo: global tab(s), encrypt all cvar strings here

	for ( size_t Iterator = 0; Iterator < MAX_TABS; Iterator++ )
	{
		pParent = ( Cvar_Parent* )g_GUI.m_Tabs[ Iterator ].Parent;

		for ( ; pParent->m_Name != 0; pParent++ )
		{
			pParent->m_Length = strlen ( pParent->m_Name );
			pParent->m_Key = rand()% 65535;
			
			for ( size_t Iterator = 0; Iterator < pParent->m_Length; Iterator++ )
			{
				pParent->m_Name[ Iterator ] ^= ( ( pParent->m_Key - ( Iterator * pParent->m_Length * 64 ) ) % 0xFF );
			}

			pGroup = pParent->m_Group;

			for ( ; pGroup->m_Name != 0; pGroup++ )
			{
				pGroup->m_Length = strlen ( pGroup->m_Name );

				pGroup->m_Key = rand()% 65535;

				for ( size_t Iterator = 0; Iterator < pGroup->m_Length; Iterator++ )
				{
					pGroup->m_Name[ Iterator ] ^= ( ( pGroup->m_Key - ( Iterator * pGroup->m_Length * 64 ) ) % 0xFF );
				}
				
				ppCvar = pGroup->m_Cvars;

				for ( ; *ppCvar; ppCvar++ )
				{
					pCvar = *ppCvar;

					if ( pCvar->m_LengthMenuString == 0 && pCvar->m_LengthIniString == 0 )
					{
						pCvar->m_LengthMenuString = strlen ( pCvar->m_MenuString );
						pCvar->m_LengthIniString = strlen ( pCvar->m_IniString );

						pCvar->m_KeyMenuString = rand()% 65535;
						pCvar->m_KeyIniString = rand()% 65535;

						for ( size_t Iterator = 0; Iterator < pCvar->m_LengthMenuString; Iterator++ )
						{
							pCvar->m_MenuString[ Iterator ] ^= ( ( pCvar->m_KeyMenuString - ( Iterator * pCvar->m_LengthMenuString * 64 ) ) % 0xFF );
						}
						for ( size_t Iterator = 0; Iterator < pCvar->m_LengthIniString; Iterator++ )
						{
							pCvar->m_IniString[ Iterator ] ^= ( ( pCvar->m_KeyIniString - ( Iterator * pCvar->m_LengthIniString * 64 ) ) % 0xFF );
						}
					}
				}
			}
		}
	}


//	HookProxy ( ( uintptr_t )OriginProxy, ClientDLL + DT_BaseEntity_m_vecOrigin_Netvarhook_RVADisplacement, ClientDLL, CSGO_CLIENTDLL_SECTION_DATA_RVA, CSGO_CLIENTDLL_SECTION_DATA_SIZE );
//	HookProxy ( ( uintptr_t )PunchAngleProxy, ClientDLL + DT_Local_m_aimPunchAngle_Netvarhook_RVADisplacement, ClientDLL, CSGO_CLIENTDLL_SECTION_DATA_RVA, CSGO_CLIENTDLL_SECTION_DATA_SIZE );
//	HookProxy ( ( uintptr_t )SimulationTimeProxy, ClientDLL + DT_BaseEntity_m_flSimulationTime_Netvarhook_CodeReferenceRVA, ClientDLL, CSGO_CLIENTDLL_SECTION_DATA_RVA, CSGO_CLIENTDLL_SECTION_DATA_SIZE );
//	HookProxy ( ( uintptr_t )EyeAnglesPitchProxy, ClientDLL + DT_CSPlayer_m_angEyeAngles_0_Netvarhook_RVADisplacement, ClientDLL, CSGO_CLIENTDLL_SECTION_DATA_RVA, CSGO_CLIENTDLL_SECTION_DATA_SIZE );
//	HookProxy ( ( uintptr_t )EyeAnglesYawProxy, ClientDLL + DT_CSPlayer_m_angEyeAngles_1_Netvarhook_RVADisplacement, ClientDLL, CSGO_CLIENTDLL_SECTION_DATA_RVA, CSGO_CLIENTDLL_SECTION_DATA_SIZE );
		
	g_Draw.m_ESPFont = g_EncryptedSurface.Get<ISurface>()->CreateFont();
	g_Draw.m_MenuFont = g_EncryptedSurface.Get<ISurface>()->CreateFont();

	g_EncryptedSurface.Get<ISurface>()->SetFontGlyphSet ( g_Draw.m_ESPFont, "Segoe UI", ESP_FONT_SIZE, 700, 0, 0, 0x200 );
	g_EncryptedSurface.Get<ISurface>()->SetFontGlyphSet ( g_Draw.m_MenuFont, "Microsoft Sans Serif", MENU_FONT_SIZE, 700, 0, 0, 0x200 );

	g_VirtualFree = GetProcAddress ( ( HMODULE )GetModuleHandle ( KERNEL32_HASH ), 0xef776d34 ); 

	return;
}
//==================================================================================================================================
#include "wipe_end.h"
//==================================================================================================================================