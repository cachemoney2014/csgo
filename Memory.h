//=============================================================================================================
extern "C" void * __cdecl memcpy ( void*, const void*, size_t );
extern "C" void * __cdecl memset ( void*, int, size_t );
//=============================================================================================================
void* __cdecl memcpy ( void* _Dest, const void* _Source, size_t _Size );
void* __cdecl memset ( void* _Dest, int _Val, size_t _Size );
//=============================================================================================================
PVOID __stdcall AllocateFromHeap ( DWORD dwSize );
void __stdcall DeallocateOffHeap ( PVOID pvMemory );
//=============================================================================================================