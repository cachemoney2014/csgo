class CEncryptedAddressPointer
{
public:
	
	void SetKey ( uintptr_t Key ){ m_Key = Key; }
		
	CEncryptedAddressPointer& operator = ( uintptr_t Address )
	{
		this->m_Address = Address ^ m_Key;
		
		return *this;
	}
				
	template<typename T>
	T* Get ( void )
	{
		return reinterpret_cast<T*>( this->m_Address ^ m_Key );
	}

	template<typename T>
	T Get2 ( void )
	{
		return reinterpret_cast<T>( this->m_Address ^ m_Key );
	}

private:

	uintptr_t m_Address;

	uintptr_t m_Key;
};
//==================================================================================================================================
class CExternalVMTHook
{
public:

	CExternalVMTHook(){};

	void Hook ( uintptr_t Function, uintptr_t Index )
	{
		m_HookedTable[ Index ] = Function;
	}

	void UnHook ( uintptr_t Index )
	{
		m_HookedTable[ Index ] = m_OriginalTable[ Index ];
	}

	void Init ( uintptr_t VTablePointer, uintptr_t TextBase, uintptr_t TextEnd )
	{
		uintptr_t Address = 0;
		uintptr_t* Pointer = 0;
		uintptr_t RDataPointer = 0, DataPointer = 0;

		m_SingletonPointer = ( uintptr_t*** )VTablePointer;

		m_OriginalDataPointer = *m_SingletonPointer;

		m_OriginalTable = **m_SingletonPointer;

		while ( m_OriginalTable[ m_VTableSize ] >= TextBase &&  m_OriginalTable[ m_VTableSize ] <= TextEnd )
		{
			m_VTableSize++;
		}
		
		m_HookedDataPointer = ( uintptr_t** )AllocateFromHeap ( sizeof ( uintptr_t ) );
				
		*m_HookedDataPointer = ( uintptr_t* )AllocateFromHeap ( sizeof ( uintptr_t ) * m_VTableSize );

		m_HookedTable = *m_HookedDataPointer;

		memcpy ( m_HookedTable, m_OriginalTable, sizeof ( uintptr_t ) * m_VTableSize );
		
		m_OriginalDataPointer = ( uintptr_t** )( ( uintptr_t )m_OriginalDataPointer ^ m_Key );

		m_OriginalTable = ( uintptr_t* )( ( uintptr_t )m_OriginalTable ^ m_Key );

		*m_SingletonPointer = m_HookedDataPointer;

		m_VTableSize = 0;
	}

	void SetKey ( uintptr_t Key )
	{
		m_Key = Key;
	}

	uintptr_t* GetOriginalVTablePointer()
	{
		return ( uintptr_t* )( ( uintptr_t )m_OriginalTable ^ m_Key );
	}

	uintptr_t* GetHookedVTablePointer()
	{
		return ( uintptr_t* )( ( uintptr_t )m_HookedTable ^ m_Key );
	}

	uintptr_t** GetOriginalDataPointer()
	{
		return ( uintptr_t** )( ( uintptr_t )m_OriginalDataPointer ^ m_Key );
	}
		
	template< typename T > inline T GetOriginalVTableMethod ( uintptr_t Index )
	{
		return ( T )GetOriginalVTablePointer()[ Index ];
	}

	uintptr_t GetHookedVTableMethod ( uintptr_t Index )
	{
		return m_HookedTable[ Index ];
	}
	
	uintptr_t* m_OriginalTable;
	uintptr_t* m_HookedTable;

	uintptr_t** m_HookedDataPointer;
	uintptr_t** m_OriginalDataPointer;
	uintptr_t*** m_SingletonPointer;
		
	uintptr_t m_VTableSize;

	uintptr_t m_Key;
};
//==================================================================================================================================
class CVMTHook
{
public:

	CVMTHook(){};

	void Hook ( uintptr_t Function, uintptr_t Index )
	{
		m_HookedTable[ Index ] = Function;
	}

	void UnHook ( uintptr_t Index )
	{
		m_HookedTable[ Index ] = m_OriginalTable[ Index ];
	}

	void UnHookAll ( void )
	{
		if ( m_Depth > 1 )
		{
			*m_SingletonPointer = m_OriginalDataPointer;

			DeallocateOffHeap ( *m_HookedDataPointer );
			DeallocateOffHeap ( m_HookedDataPointer );

			DeallocateOffHeap ( m_HookedTable );
		}
		else
		{
			*m_OriginalDataPointer = m_OriginalTable;

			DeallocateOffHeap ( m_HookedTable );
		}
	}

	void Init ( uintptr_t VTablePointer )
	{
		uintptr_t Address = 0;
		uintptr_t* Pointer = 0;
		uintptr_t RDataPointer = 0, DataPointer = 0;

		Address = VTablePointer;
						
		while ( 1 )
		{
			Pointer = ( uintptr_t* )Address;
						
			if ( *Pointer >= m_DataBase && *Pointer <= m_DataEnd )
			{
				DataPointer = *Pointer;
			}
			else if ( *Pointer >= m_RDataBase && *Pointer <= m_RDataEnd )
			{
				RDataPointer = *Pointer;
			}
			else
			{
				break;
			}

			Address = *Pointer;
	
			m_Depth++;
		}
		if ( m_Depth != 0 && m_Depth > 1 )
		{						
			m_HookedDataPointer = ( uintptr_t** )AllocateFromHeap ( sizeof ( uintptr_t ) * 0x1000 );

			memcpy ( m_HookedDataPointer, ( void* )DataPointer, sizeof ( uintptr_t ) * 0x1000 );

			*m_HookedDataPointer = ( uintptr_t* )AllocateFromHeap ( sizeof ( uintptr_t ) * m_VTableSize );

			m_HookedTable = *m_HookedDataPointer;

			m_OriginalTable = ( uintptr_t* )RDataPointer;

			memcpy ( m_HookedTable, m_OriginalTable, sizeof ( uintptr_t ) * m_VTableSize );	

			m_OriginalDataPointer = ( uintptr_t** )DataPointer;
			
			Address = VTablePointer;

			m_SingletonPointer = ( uintptr_t*** )VTablePointer;

			for ( uintptr_t Iterator = 0; Iterator < m_Depth; Iterator++ )
			{
				Pointer = ( uintptr_t* )Address;

				if ( *Pointer == DataPointer )
				{
					*Pointer = ( uintptr_t )m_HookedDataPointer;

					m_SingletonPointer = ( uintptr_t*** )Pointer;

					break;
				}

				Address = *Pointer;
			}

			m_OriginalDataPointer = ( uintptr_t** )( DataPointer ^ m_Key );

			m_OriginalTable = ( uintptr_t* )( RDataPointer ^ m_Key );
		}
		else
		{
			Pointer = ( uintptr_t* )VTablePointer;

			m_OriginalDataPointer = ( uintptr_t** )VTablePointer;

			m_OriginalTable = ( uintptr_t* )RDataPointer;

			m_HookedTable = ( uintptr_t* )AllocateFromHeap ( sizeof ( uintptr_t ) * m_VTableSize );

			memcpy ( m_HookedTable, m_OriginalTable, sizeof ( uintptr_t ) * m_VTableSize );

			*Pointer = ( uintptr_t )m_HookedTable;

			m_OriginalDataPointer = ( uintptr_t** )( VTablePointer ^ m_Key );

			m_OriginalTable = ( uintptr_t* )( RDataPointer ^ m_Key );
		}

		m_ModuleBase = m_TextBase = m_TextEnd =	m_RDataBase = m_RDataEnd = m_DataBase = m_DataEnd = m_Depth = m_VTableSize = 0;
	}

	void SetInformation ( uintptr_t ModuleBase, uintptr_t TextRVABase, uintptr_t TextRVAEnd,
		uintptr_t RDataRVABase, uintptr_t RDataRVAEnd, uintptr_t DataRVABase, uintptr_t DataRVAEnd,
		uintptr_t VTableSize )
	{
		m_VTableSize = VTableSize;

		m_ModuleBase = ModuleBase;
		m_TextBase = ModuleBase + TextRVABase;
		m_TextEnd = ModuleBase + TextRVABase + TextRVAEnd;

		m_RDataBase = ModuleBase + RDataRVABase;
		m_RDataEnd = ModuleBase + RDataRVABase + RDataRVAEnd;

		m_DataBase = ModuleBase + DataRVABase;
		m_DataEnd = ModuleBase + DataRVABase + DataRVAEnd;
	}

	void SetKey ( uintptr_t Key )
	{
		m_Key = Key;
	}

	uintptr_t* GetOriginalVTablePointer()
	{
		return ( uintptr_t* )( ( uintptr_t )m_OriginalTable ^ m_Key );
	}

	uintptr_t* GetHookedVTablePointer()
	{
		return ( uintptr_t* )( ( uintptr_t )m_HookedTable ^ m_Key );
	}

	uintptr_t** GetOriginalDataPointer()
	{
		return ( uintptr_t** )( ( uintptr_t )m_OriginalDataPointer ^ m_Key );
	}
		
	template< typename T > inline T GetOriginalVTableMethod ( uintptr_t Index )
	{
		return ( T )GetOriginalVTablePointer()[ Index ];
	}

	uintptr_t GetHookedVTableMethod ( uintptr_t Index )
	{
		return m_HookedTable[ Index ];
	}
	
	uintptr_t* m_OriginalTable;
	uintptr_t* m_HookedTable;

	uintptr_t** m_HookedDataPointer;
	uintptr_t** m_OriginalDataPointer;
	uintptr_t*** m_SingletonPointer;
		
	uintptr_t m_VTableSize;

	uintptr_t m_TextBase, m_TextEnd, m_RDataBase, m_RDataEnd, m_DataBase, m_DataEnd, m_ModuleBase;
	
	uintptr_t m_Depth;

	uintptr_t m_Key;
};
//==================================================================================================================================
uintptr_t Allocate ( uintptr_t Size );
uintptr_t HookEntry ( uintptr_t* Table, uintptr_t NewFunction, uintptr_t Index );
//==================================================================================================================================