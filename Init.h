//==================================================================================================================================
void DLLStartup ( uintptr_t ModuleBase, LPVOID lpReserved );
//==================================================================================================================================
void Setup ( void );
//==================================================================================================================================
extern CVMTHook ClientModeHook;
extern CExternalVMTHook VGUIMatSurfaceVGUIPanel009;
extern CExternalVMTHook VClient018;
extern CExternalVMTHook IVEngineClient014;
//==================================================================================================================================
extern CEncryptedAddressPointer g_EncryptedPanel;
extern CEncryptedAddressPointer g_EncryptedSurface;
extern CEncryptedAddressPointer g_EncryptedEngineClient;
extern CEncryptedAddressPointer g_EncryptedGetCursorPos;
extern CEncryptedAddressPointer g_EncryptedInputSystem;
extern CEncryptedAddressPointer g_EncryptedGetAsyncKeyState;
extern CEncryptedAddressPointer g_GetKeyboardState;
extern CEncryptedAddressPointer g_GetKeyNameText;
extern CEncryptedAddressPointer g_EncryptedMBSTOWCS;
extern CEncryptedAddressPointer g_MapVirtualKey;
//==================================================================================================================================
typedef PVOID ( NTAPI* RtlAllocateHeap_t )( PVOID HeapHandle, ULONG Flags, ULONG Size );
typedef BOOLEAN ( NTAPI* RtlFreeHeap_t )( PVOID HeapHandle, ULONG Flags, PVOID MemoryPointer );
typedef int ( __cdecl* rand_t )( void );
typedef void ( __cdecl* srand_t )( unsigned int seed );
//==================================================================================================================================