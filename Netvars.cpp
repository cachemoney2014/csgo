//=============================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "Client.h"
//==================================================================================================================================
#include "wipe.h"
#include "wipe_start.h"
//=============================================================================================================
void HookProxy ( uintptr_t Function, uintptr_t Address, uintptr_t Base, uintptr_t DataBase, uintptr_t DataSize )
{
	if ( Address >= ( Base + DataBase ) && Address <= ( Base + DataBase + DataSize ) )
	{
		*( uintptr_t* )Address = Function;
	}
}
//=============================================================================================================
#include "wipe_end.h"
//=============================================================================================================
void OriginProxy( CRecvProxyData *pData, C_BaseEntity *pBaseEntityt, void *pOut)
{
	Vector Origin = pData->m_Value.m_Vector;

	Vector* Out = ( Vector* )pOut;
	
	*Out = Origin;
}
//=============================================================================================================
void PunchAngleProxy( CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut)
{
	Vector Origin = pData->m_Value.m_Vector;

	Vector* Out = ( Vector* )pOut;
	
	*Out = Origin;
}
//=============================================================================================================
void SimulationTimeProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut)
{
	float* Out = ( float* )pOut;

	int Index = pBaseEntity->GetIndex();

	CSGO_Player.m_Data[Index].m_LastSimulationTime = CSGO_Player.m_Data[Index].m_SimulationTime;

	CSGO_Player.m_Data[Index].m_SimulationTime = *Out;
		
	*Out = pData->m_Value.m_Float;
}
//=============================================================================================================
void EyeAnglesPitchProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut)
{
	float* Out = ( float* )pOut;
	
	*Out = pData->m_Value.m_Float;
}
//=============================================================================================================
void EyeAnglesYawProxy(CRecvProxyData *pData, C_BaseEntity *pBaseEntity, void *pOut)
{
	float* Out = ( float* )pOut;

	*Out = pData->m_Value.m_Float;
}
//=============================================================================================================