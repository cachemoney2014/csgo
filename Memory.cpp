//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "Memory.h"
//==================================================================================================================================
#pragma function ( memcpy, memset )
//==================================================================================================================================
void* __cdecl memcpy ( void* _Dest, const void* _Source, size_t _Size )
{
	unsigned int uiBufferSize, uiRemainderSource, uiRemainderDest;

	PBYTE pbDest = ( PBYTE )_Dest;
	PBYTE pbSource = ( PBYTE )_Source;

	PDWORD pdwDest = ( PDWORD )_Dest;
	PDWORD pdwSource = ( PDWORD )_Source;

	uiRemainderSource = ( sizeof ( DWORD ) - ( ( DWORD )_Source & ( sizeof ( DWORD ) -1 ) ) );

	uiRemainderDest = ( sizeof ( DWORD ) - ( ( DWORD )_Dest & ( sizeof ( DWORD ) -1 ) ) );

	if ( uiRemainderSource == uiRemainderDest
		&& uiRemainderSource != 0 && _Size >= uiRemainderSource  )
	{		
		_Size -= uiRemainderSource;

		while ( uiRemainderSource-- )
		{
			*pbDest++ = *pbSource++;
		}

		pdwDest = ( PDWORD )pbDest;
		pdwSource = ( PDWORD )pbSource;
	}

	// see how many dwords can fit in the space
		
	uiBufferSize = _Size >> 2;

	if ( uiBufferSize != 0 )
	{
		_Size -= ( uiBufferSize << 2 );

		while ( uiBufferSize-- )
		{
			*pdwDest++ = *pdwSource++;
		}
	}
	if ( _Size != 0 ) // we still have space left over
	{		
		pbDest = ( PBYTE )pdwDest;
		pbSource = ( PBYTE )pdwSource;

		while ( _Size-- )
		{
			*pbDest++ = *pbSource++;
		}
	}

	return _Dest;
}
//==================================================================================================================================
void* __cdecl memset ( void* _Dest, int _Val, size_t _Size )
{
	unsigned int uiBufferSize, uiRemainder;
	
	PDWORD pdwDest = ( PDWORD )_Dest;

	PBYTE pbDest = ( PBYTE )_Dest;
	
	uiRemainder = ( sizeof ( DWORD ) - ( ( DWORD )_Dest & ( sizeof ( DWORD ) -1 ) ) );
		
	if ( uiRemainder != 0 && _Size >= uiRemainder ) // unaligned memory
	{		
		_Size -= uiRemainder;
			
		while ( uiRemainder-- ) // get us on aligned memory
		{
			*pbDest++ = _Val;
		}

		pdwDest = ( PDWORD )pbDest;
	}
	
	// see how many dwords can fit in the space
	
	uiBufferSize = _Size >> 2;

	if ( uiBufferSize != 0 )
	{
		_Size -= ( uiBufferSize << 2 );

		while ( uiBufferSize-- )
		{
			*pdwDest++ = _Val;
		}
	}
	
	if ( _Size != 0 ) // we still have space left over
	{
		pbDest = ( PBYTE )pdwDest;
				
		while ( _Size-- )
		{
			*pbDest++ = _Val;
		}
	}
		
	return _Dest;
}