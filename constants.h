//==================================================================================================================================
extern float g_PI;
extern float g_2PI;
extern float g_180;
extern float g_RadiansToDegrees;
extern float g_DegreesToRadians;
extern float g_360;
extern float g_270;
extern float g_90;
extern float g_1;
extern float g_Negative1;
extern float g_999999;
extern float g_001;
extern float g_0;
extern float g_255;
extern float g_022;
//==================================================================================================================================