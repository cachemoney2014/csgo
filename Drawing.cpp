//==================================================================================================================================
#include "Required.h"
#include "Memory.h"
//==================================================================================================================================
#include "Drawing.h"
#include "Hook.h"
#include "Init.h"
#include "Strings.h"
//==================================================================================================================================
typedef size_t ( __cdecl* mbstowcs_t )(  
   wchar_t *wcstr,  
   const char *mbstr,  
   size_t count );
//==================================================================================================================================
CDraw g_Draw;
//==================================================================================================================================
float  CDraw::GetSliderStep ( float MaxValue, int Width )
{
	float Step;

	if ( MaxValue < Width )
	{
		Step = ( float )Width / MaxValue;
	}
	else
	{
		Step = MaxValue / ( float )Width;
	}

	return Step;
}
//==================================================================================================================================
void CDraw::DrawString ( int x, int y, Color_t& Color, char* Text, uintptr_t Align, uintptr_t Font )
{
	wchar_t wString[ MAX_PATH ];

	int Width, Height;

	memset ( wString, 0, sizeof ( wString ) );

	g_EncryptedMBSTOWCS.Get2<mbstowcs_t>()(wString, Text, strlen ( Text ) );

	g_EncryptedSurface.Get<ISurface>()->GetTextSize ( Font, wString, Width, Height );

	if ( Align & AlignRight )
		x -= Width;
	else if ( Align & AlignCenterH )
		x -= Width / 2;
	if ( Align & AlignCenterV )
		y -= Height / 2;

	g_EncryptedSurface.Get<ISurface>()->DrawSetTextColor ( Color.r, Color.g, Color.b, Color.a );
	g_EncryptedSurface.Get<ISurface>()->DrawSetTextFont ( Font );
	g_EncryptedSurface.Get<ISurface>()->DrawSetTextPos ( x, y );
	g_EncryptedSurface.Get<ISurface>()->DrawPrintText ( wString, wstrlen ( wString ) );
}
//==================================================================================================================================
void CDraw::DrawStringEncrypted ( int x, int y, Color_t& Color, char* Text, uintptr_t Length, uintptr_t Key, int &Width, int &Height, uintptr_t Align, uintptr_t Font )
{
	wchar_t wString[ MAX_PATH ];
	char String[ MAX_PATH ];

	memset ( &String, 0, MAX_PATH );
	memset ( wString, 0, sizeof ( wString ) );
	
	memcpy ( &String, Text, Length );
	
	for ( size_t Iterator = 0; Iterator < Length; Iterator++ )
	{
		String[ Iterator ] ^= ( ( Key - ( Iterator * Length * 64 ) ) % 0xFF );
	}

	g_EncryptedMBSTOWCS.Get2<mbstowcs_t>()(wString, String, strlen ( String ) );

	g_EncryptedSurface.Get<ISurface>()->GetTextSize ( Font, wString, Width, Height );

	if ( Align & AlignRight )
		x -= Width;
	else if ( Align & AlignCenterH )
		x -= Width / 2;
	if ( Align & AlignCenterV )
		y -= Height / 2;
			
	g_EncryptedSurface.Get<ISurface>()->DrawSetTextColor ( Color.r, Color.g, Color.b, Color.a );
	g_EncryptedSurface.Get<ISurface>()->DrawSetTextFont ( Font );
	g_EncryptedSurface.Get<ISurface>()->DrawSetTextPos ( x, y );
	g_EncryptedSurface.Get<ISurface>()->DrawPrintText ( wString, Length );

	memset ( &String, 0, Length );
	memset ( &wString, 0, Length * 2 );
}
//==================================================================================================================================
void CDraw::DrawFilledRect ( int x, int y, int w, int h, Color_t& Color )
{
	g_EncryptedSurface.Get<ISurface>()->DrawSetColor ( Color.r, Color.g, Color.b, Color.a );
	g_EncryptedSurface.Get<ISurface>()->DrawFilledRect ( x, y, x + w, y + h );
}
//==================================================================================================================================
void CDraw::DrawOutlinedRect ( int x, int y, int w, int h, Color_t& Color )
{
	g_EncryptedSurface.Get<ISurface>()->DrawSetColor ( Color.r, Color.g, Color.b, Color.a );
	g_EncryptedSurface.Get<ISurface>()->DrawOutlinedRect ( x, y, x + w, y + h );
}
//==================================================================================================================================
void CDraw::DrawLine ( int x, int y, int x1, int y1, Color_t& Color )
{
	g_EncryptedSurface.Get<ISurface>()->DrawSetColor ( Color.r, Color.g, Color.b, Color.a );
	g_EncryptedSurface.Get<ISurface>()->DrawLine ( x, y, x1, y1 );
}
//==================================================================================================================================
void CDraw::GradientV ( int x, int y, int w, int h, Color_t& c1, Color_t& c2)
{
	Color_t c3;

	DrawFilledRect ( x, y, w, h, c1 );
	BYTE first = c2.r;
	BYTE second = c2.g;
	BYTE third = c2.b;

	unsigned char NewAlpha;
	
	for ( int i = 0; i < h; i++ )
	{
		NewAlpha = g_255 * ( ( float )i / ( float )h );

		c3.Init ( first, second, third, NewAlpha );

		DrawFilledRect ( x, y + i, w, 1, c3 );
	}
}
//==================================================================================================================================
void CDraw::GradientH(int x, int y, int w, int h, Color_t& c1, Color_t& c2)
{
	Color_t c3;

	DrawFilledRect ( x, y, w, h, c1 );
	BYTE first = c2.r;
	BYTE second = c2.g;
	BYTE third = c2.b;

	unsigned char NewAlpha;

	for ( int i = 0; i < w; i++ )
	{
		NewAlpha = g_255 * ( ( float )i / ( float )w );

		c3.Init ( first, second, third, NewAlpha );

		DrawFilledRect ( x + i, y, 1, h, c3 );
	}
}
//==================================================================================================================================