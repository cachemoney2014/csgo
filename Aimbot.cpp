//==================================================================================================================================
#include "Required.h"
#include "Aimbot.h"
#include "Client.h"
#include "Cvars.h"
//==================================================================================================================================
Aimbot_t CSGO_AimbotData;
//==================================================================================================================================
void CSGO_Aimbot::FindTargetDistance()
{
	float LowestDistance = g_999999;
	
	for ( uintptr_t Iterator = 0; Iterator < CSGO_Player.m_PlayerCount; Iterator++ )
	{
		if ( Iterator != CSGO_LocalPlayer.m_Data.m_Index )
		{
			if ( CSGO_Player.m_Data[ Iterator ].m_Distance < LowestDistance )
			{
				LowestDistance = CSGO_Player.m_Data[ Iterator ].m_Distance;

				CSGO_AimbotData.m_Target = Iterator;
			}
		}
	}
}
//==================================================================================================================================
void CSGO_Aimbot::FindTargetFOV()
{
	float LowestFOV = g_999999;

	for ( uintptr_t Iterator = 0; Iterator < CSGO_Player.m_PlayerCount; Iterator++ )
	{
		if ( Iterator != CSGO_LocalPlayer.m_Data.m_Index )
		{
			if ( CSGO_Player.m_Data[ Iterator ].m_FOVSqr < LowestFOV )
			{
				LowestFOV = CSGO_Player.m_Data[ Iterator ].m_FOVSqr;

				CSGO_AimbotData.m_Target = Iterator;
			}
		}
	}
}
//==================================================================================================================================
void CSGO_Aimbot::FindTargetDamage()
{
	for ( uintptr_t Iterator = 0; Iterator < CSGO_Player.m_PlayerCount; Iterator++ )
	{
		if ( Iterator != CSGO_LocalPlayer.m_Data.m_Index )
		{

		}
	}
}
//==================================================================================================================================
void CSGO_Aimbot::FindTargetSmart()
{
	for ( uintptr_t Iterator = 0; Iterator < CSGO_Player.m_PlayerCount; Iterator++ )
	{
		if ( Iterator != CSGO_LocalPlayer.m_Data.m_Index )
		{

		}
	}
}
//==================================================================================================================================
bool CSGO_Aimbot::IsAimingAtMe ( float* MyOrigin, float* EnemyOrigin, float* EnemyAngles )
{
	return false;
}
//==================================================================================================================================
void CSGO_Aimbot::FindTarget()
{
	if ( cvars::aim_rage_lock.m_Value && CSGO_AimbotData.m_Target != g_Negative1 )
	{
		return;
	}

	switch ( cvars::aim_rage_mode.m_MinValue )
	{
		case 1:
		{
			FindTargetDistance();
		}
		break;

		case 2:
		{
			FindTargetFOV();
		}
		break;

		case 3:
		{
			FindTargetDamage();
		}
		break;

		case 4:
		{
			FindTargetSmart();
		}
		break;
	}
}
//==================================================================================================================================