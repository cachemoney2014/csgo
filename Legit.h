//==================================================================================================================================
char LegitAimbot[] = "Legit Aimbot"; // parent names
char Triggerbot[] = "Triggerbot";
char TriggerbotFilter[] = "Triggerbot Filter";
char Pistols[] = "Pistols";
char Rifles[] = "Rifles";
char Snipers[] = "Snipers";
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
char EnableLegitAimbot[] = "Enable Aimbot"; // cvar titles
char LegitAimKey[] = "Aim Key";
char LegitAutoShoot[] = "Auto Shoot";
char LegitFriendlyFire[] = "FriendlyFire";
char LegitSpeedLimit[] = "Speed Limit";
//==================================================================================================================================
//==================================================================================================================================
char AimLegitAimbot[] = "aim_legitaimbot"; // cvar names
char AimLegitAimKey[] = "aim_legitaimkey";
char AimLegitAutoShoot[] = "aim_legitautoshoot";
char AimLegitFriendlyFire[] = "aim_legitfriendlyfire";
char AimLegitSpeed[] = "aim_legit_speed";
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
char EnableTriggerbot[] = "Enable Triggerbot";
char TriggerbotKey[] = "Triggerbot Key";
char TriggerbotDelay[] = "Triggerbot Delay";
char TriggerbotHitchance[] = "Hitchance";
//==================================================================================================================================
//==================================================================================================================================
char AimTriggerbot[] = "aim_triggerbot";
char AimTriggerbotKey[] = "aim_triggerbot_key";
char AimTriggerbotDelay[] = "aim_triggerbot_delay";
char AimTriggerbotHitchance[] = "aim_triggerbot_hitchance";
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
char LegitPistolFOV[] = "FOV";
char LegitPistolRecoil[] = "Recoil";
char LegitPistolHitcapsule[] = "Hitcapsule";
char LegitPistolAutowall[] = "Autowall";
//==================================================================================================================================
//==================================================================================================================================
char AimLegitPistolFOV[] = "aim_legit_pistol_fov";
char AimLegitPistolRecoil[] = "aim_legit_pistol_recoil";
char AimLegitPistolHitcapsule[] = "aim_legit_pistol_hitcapsule";
char AimLegitPistolAutowall[] = "aim_legit_pistol_autowall";
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
char LegitRifleFOV[] = "FOV";
char LegitRifleRecoil[] = "Recoil";
char LegitRifleHitcapsule[] = "Hitcapsule";
char LegitRifleAutowall[] = "Autowall";
//==================================================================================================================================
//==================================================================================================================================
char AimLegitRifleFOV[] = "aim_legit_Rifle_fov";
char AimLegitRifleRecoil[] = "aim_legit_Rifle_recoil";
char AimLegitRifleHitcapsule[] = "aim_legit_Rifle_hitcapsule";
char AimLegitRifleAutowall[] = "aim_legit_Rifle_autowall";
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
char LegitSniperFOV[] = "FOV";
char LegitSniperRecoil[] = "Recoil";
char LegitSniperHitcapsule[] = "Hitcapsule";
char LegitSniperAutowall[] = "Autowall";
//==================================================================================================================================
//==================================================================================================================================
char AimLegitSniperFOV[] = "aim_legit_Sniper_fov";
char AimLegitSniperRecoil[] = "aim_legit_Sniper_recoil";
char AimLegitSniperHitcapsule[] = "aim_legit_Sniper_hitcapsule";
char AimLegitSniperAutowall[] = "aim_legit_Sniper_autowall";
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
char LegitHead[] = "Head";
char LegitNeck[] = "Neck";
char LegitHighSpine[] = "High Spine";
char LegitLowSpine[] = "Low Spine";
char LegitPelvis[] = "Pelvis";
char LegitThigh[] = "Thigh";
char LegitUpperArm[] = "Upper Arm";
char LegitForeArm[] = "Fore Arm";
char LegitCalf[] = "Calf";
//==================================================================================================================================
//==================================================================================================================================
char TriggerSpotHead[] = "trig_legit_spot_head";
char TriggerSpotNeck[] = "trig_legit_spot_neck";
char TriggerSpotHighSpine[] = "trig_legit_spot_highspine";
char TriggerSpotLowSpine[] = "trig_legit_spot_lowspine";
char TriggerSpotPelvis[] = "trig_legit_spot_pelvis";
char TriggerSpotThigh[] = "trig_legit_spot_thigh";
char TriggerSpotUpperArm[] = "trig_legit_spot_upperarm";
char TriggerSpotForeArm[] = "trig_legit_spot_forearm";
char TriggerSpotCalf[] = "trig_legit_spot_calf";
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================