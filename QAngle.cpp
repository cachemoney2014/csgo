#include "Required.h"

void QAngle::AngleVectors ( Vector* Forward, Vector* Right, Vector* Up )
{
	float sp, sy, sr, cp, cy, cr, radx, rady, radz;

	radx = x * ( g_2PI / g_360 );
	rady = y * ( g_2PI / g_360 );
	radz = z * ( g_2PI / g_360 );

	sp = _sin ( radx ); 
	sy = _sin ( rady ); 
	sr = _sin ( radz ); 

	cp = _cos ( radx );
	cy = _cos ( rady );
	cr = _cos ( radz );

	if ( Forward )
	{
		Forward->x = cp * cy;
		Forward->y = cp * sy;
		Forward->z = -sp;
	}

	if ( Right )
	{
		Right->x = -sr * sp * cy + -cr * -sy;
		Right->y = -sr * sp * sy + -cr * cy;
		Right->z = -sr * cp;
	}

	if ( Up )
	{
		Up->x = cr * sp * cy + -sr * -sy;
		Up->y = cr * sp * sy + -sr * cy;
		Up->z = cr * cp;
	}
}

void QAngle::AngleVectorsTranspose ( Vector* Forward, Vector* Right, Vector* Up )
{
	float sp, sy, sr, cp, cy, cr, radx, rady, radz;

	radx = x * ( g_2PI / g_360 );
	rady = y * ( g_2PI / g_360 );
	radz = z * ( g_2PI / g_360 );
		
	sp = _sin ( radx ); 
	sy = _sin ( rady ); 
	sr = _sin ( radz ); 

	cp = _cos ( radx );
	cy = _cos ( rady );
	cr = _cos ( radz );

	if ( Forward )
	{
		Forward->x = cp * cy;
		Forward->y = sr * sp * cy + cr * -sy;
		Forward->z = cr * sp * cy + -sr * -sy;
	}

	if ( Right )
	{
		Right->x = cp * sy;
		Right->y = sr * sp * sy + cr * cy;
		Right->z = cr * sp * sy + -sr * cy;
	}

	if ( Up )
	{
		Up->x = -sp;
		Up->y = sr * cp;
		Up->z = cr * cp;
	}
}